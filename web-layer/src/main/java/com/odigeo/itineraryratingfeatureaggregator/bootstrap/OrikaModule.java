package com.odigeo.itineraryratingfeatureaggregator.bootstrap;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.odigeo.itineraryratingfeatureaggregator.bean.BaggageConditions;
import com.odigeo.itineraryratingfeatureaggregator.bean.BaggageDescriptor;
import com.odigeo.itineraryratingfeatureaggregator.bean.Itinerary;
import com.odigeo.itineraryratingfeatureaggregator.bean.ItineraryShoppingItem;
import com.odigeo.itineraryratingfeatureaggregator.bean.ModelEvaluationRequested;
import com.odigeo.itineraryratingfeatureaggregator.bean.Money;
import com.odigeo.itineraryratingfeatureaggregator.bean.SearchCriteria;
import com.odigeo.itineraryratingfeatureaggregator.bean.SearchResultsStatistics;
import com.odigeo.itineraryratingfeatureaggregator.bean.Section;
import com.odigeo.itineraryratingfeatureaggregator.bean.Segment;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@SuppressWarnings("PMD.CouplingBetweenObjects")
public class OrikaModule extends AbstractModule {

    @Provides
    @Singleton
    public MapperFacade getMapper() {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

        mappingsForItineraryShoppingItem(mapperFactory);
        mappingsForModelEvaluationRequested(mapperFactory);

        return mapperFactory.getMapperFacade();
    }

    private void mappingsForModelEvaluationRequested(MapperFactory mapperFactory) {
        mapperFactory.classMap(ModelEvaluationRequested.class, com.odigeo.retail.meitineraryrating.request.domainevent.ModelEvaluationRequested.class)
                .byDefault()
                .register();

        mapperFactory.classMap(SearchResultsStatistics.class, com.odigeo.retail.meitineraryrating.request.domainevent.SearchResultsStatistics.class)
                .byDefault()
                .register();

        mapperFactory.classMap(SearchCriteria.class, com.odigeo.retail.meitineraryrating.request.domainevent.SearchCriteria.class)
                .byDefault()
                .register();

    }

    private void mappingsForItineraryShoppingItem(MapperFactory mapperFactory) {
        mapperFactory.classMap(ItineraryShoppingItem.class, com.odigeo.retail.edreamssite.shoppingcart.domainevents.ItineraryShoppingItem.class)
                .byDefault()
                .register();

        mapperFactory.classMap(Itinerary.class, com.odigeo.retail.edreamssite.shoppingcart.domainevents.Itinerary.class)
                .byDefault()
                .register();

        mapperFactory.classMap(Money.class, com.odigeo.retail.edreamssite.shoppingcart.domainevents.Money.class)
                .byDefault()
                .register();

        mapperFactory.classMap(Segment.class, com.odigeo.retail.edreamssite.shoppingcart.domainevents.Segment.class)
                .byDefault()
                .register();

        mapperFactory.classMap(BaggageConditions.class, com.odigeo.retail.edreamssite.shoppingcart.domainevents.BaggageConditions.class)
                .byDefault()
                .register();

        mapperFactory.classMap(BaggageDescriptor.class, com.odigeo.retail.edreamssite.shoppingcart.domainevents.BaggageDescriptor.class)
                .byDefault()
                .register();

        mapperFactory.classMap(Section.class, com.odigeo.retail.edreamssite.shoppingcart.domainevents.Section.class)
                .byDefault()
                .register();
    }

    @Override
    protected void configure() {
        //nothing to do here
    }
}
