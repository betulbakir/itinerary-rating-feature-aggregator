package com.odigeo.itineraryratingfeatureaggregator.bootstrap.healthcheck;

import com.codahale.metrics.health.HealthCheck;
import com.odigeo.persistance.cache.impl.distributed.RedisClientFactory;
import com.odigeo.persistance.cache.impl.distributed.SerializableSerializableRedisCodec;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisConnectionException;
import io.lettuce.core.api.StatefulRedisConnection;

import java.io.Serializable;

public class RedisHealthCheck extends HealthCheck {

    public static final String NAME = "Redis-cache";

    private static final int PORT = 6379;
    private static final String HOST_NAME = "itinerary-rating-feature-aggregator-00-redis";
    private static final int PING_TIMEOUT_MILIS = 50;

    private StatefulRedisConnection<Serializable, Serializable> connection;

    private final RedisClient redisClient;

    public RedisHealthCheck() {
        this.redisClient = new RedisClientFactory().newInstance(HOST_NAME, PORT, PING_TIMEOUT_MILIS);
    }

    @Override
    protected Result check() {
        String message = getConnection().sync().ping();
        return HealthCheck.Result.healthy(message);
    }

    private StatefulRedisConnection<Serializable, Serializable> getConnection() throws RedisConnectionException {
        synchronized (this) {
            if (connection == null) {
                try {
                    connection = redisClient.connect(new SerializableSerializableRedisCodec());
                    return connection;
                } catch (RedisConnectionException e) {
                    throw new RedisConnectionException(String.format("Exception initializing Redis client (%s)", e.getMessage()), e);
                }
            }
            return connection;
        }
    }
}
