package com.odigeo.itineraryratingfeatureaggregator.bootstrap.healthcheck;

import com.odigeo.commons.messaging.KafkaTopicHealthCheck;

public class VisitConsumerHealthCheck extends KafkaTopicHealthCheck {

    public static final String NAME = "Kafka-consumer-visit-engine";

    private static final String TOPIC_NAME = "VISIT_INFORMATION_v1";

    public VisitConsumerHealthCheck() {
        super(TOPIC_NAME);
    }
}
