package com.odigeo.itineraryratingfeatureaggregator.bootstrap;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.config.files.ConfigurationFilesManager;
import com.odigeo.commons.messaging.Consumer;
import com.odigeo.commons.messaging.JsonDeserializer;
import com.odigeo.commons.messaging.KafkaConsumer;
import com.odigeo.commons.messaging.domain.events.DomainEvent;
import com.odigeo.commons.messaging.domain.events.DomainEventDeserializer;
import com.odigeo.commons.monitoring.dump.ConfiguredBeanDumpState;
import com.odigeo.commons.monitoring.dump.DumpServlet;
import com.odigeo.commons.monitoring.dump.DumpStateRegistry;
import com.odigeo.commons.monitoring.dump.SafePrintDumpState;
import com.odigeo.commons.monitoring.healthcheck.HealthCheckServlet;
import com.odigeo.commons.monitoring.metrics.MetricsManager;
import com.odigeo.commons.monitoring.metrics.reporter.ReporterStatus;
import com.odigeo.commons.rest.monitoring.dump.AutoDumpStateRegister;
import com.odigeo.commons.rest.monitoring.dump.ServiceDumpState;
import com.odigeo.commons.rest.monitoring.healthcheck.AutoHealthCheckRegister;
import com.odigeo.commons.rest.monitoring.healthcheck.ServiceHealthCheck;
import com.odigeo.geoapi.v4.client.module.ItineraryLocationServiceCacheModule;
import com.odigeo.itineraryratingfeatureaggregator.bootstrap.dump.ModelEvaluationCacheDump;
import com.odigeo.itineraryratingfeatureaggregator.bootstrap.dump.VisitCacheDump;
import com.odigeo.itineraryratingfeatureaggregator.bootstrap.healthcheck.ItineraryRatingConsumerHealthCheck;
import com.odigeo.itineraryratingfeatureaggregator.bootstrap.healthcheck.RedisHealthCheck;
import com.odigeo.itineraryratingfeatureaggregator.bootstrap.healthcheck.ShoppingItemAddedConsumerHealthCheck;
import com.odigeo.itineraryratingfeatureaggregator.bootstrap.healthcheck.VisitConsumerHealthCheck;
import com.odigeo.itineraryratingfeatureaggregator.messaging.consumer.ModelEvaluationRequestedProcessor;
import com.odigeo.itineraryratingfeatureaggregator.messaging.consumer.ShoppingItemAddedProcessor;
import com.odigeo.itineraryratingfeatureaggregator.messaging.consumer.VisitMessageProcessor;
import com.odigeo.itineraryratingfeaturehistory.api.v1.client.ItineraryRatingFeatureHistoryServiceDefaultModule;
import com.odigeo.retail.edreamssite.shoppingcart.domainevents.ShoppingItemAdded;
import com.odigeo.retail.meitineraryrating.request.domainevent.ModelEvaluationRequested;
import com.odigeo.tracking.TrackingService;
import com.odigeo.visit.message.v1.VisitMessage;
import org.apache.avro.specific.SpecificRecord;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jboss.vfs.VFS;
import org.jboss.vfs.VFSUtils;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

public class ContextListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(ContextListener.class);

    private static final String LOG4J_FILENAME = "/log4j.properties";
    private static final long LOG4J_WATCH_DELAY_MS = 1800000L;

    private Consumer<DomainEvent<SpecificRecord>> modelEvaluationRequestedConsumer;
    private Consumer<DomainEvent<SpecificRecord>> shoppingItemAddedConsumer;
    private Consumer<VisitMessage> visitConsumer;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext servletContext = event.getServletContext();
        DumpStateRegistry dumpStateRegistry = new DumpStateRegistry();
        HealthCheckRegistry healthCheckRegistry = registerHealthChecks(servletContext);
        servletContext.log("HealthCheck initialized");

        servletContext.log("Bootstrapping .....");

        initLog4J();
        servletContext.log("Log4j initialized");

        registerDumpStates(servletContext);
        servletContext.log("DumpState registered");

        initConfigurationEngine(healthCheckRegistry, dumpStateRegistry);
        servletContext.log("ConfigurationEngine initialized");


        initMetrics();
        servletContext.log("Metrics initialized");

        launchConsumers();

        servletContext.log("Bootstrapping finished!");
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        try {
            this.modelEvaluationRequestedConsumer.shutdown();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        try {
            this.shoppingItemAddedConsumer.shutdown();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        try {
            this.visitConsumer.shutdown();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        event.getServletContext().log("context destroyed");
    }

    private void registerDumpStates(ServletContext servletContext) {
        DumpStateRegistry dumpStateRegistry = new DumpStateRegistry();
        dumpStateRegistry.add(new ServiceDumpState(TrackingService.class));
        dumpStateRegistry.add(new ModelEvaluationCacheDump());
        dumpStateRegistry.add(new VisitCacheDump());

        final Collection<Class> configurationClasses = getConfigurations();
        for (Class clazz : configurationClasses) {
            dumpStateRegistry.add(new SafePrintDumpState(new ConfiguredBeanDumpState(clazz)));
        }

        servletContext.setAttribute(DumpServlet.REGISTRY_KEY, dumpStateRegistry);
    }

    private HealthCheckRegistry registerHealthChecks(ServletContext context) {
        final HealthCheckRegistry healthCheckRegistry = new HealthCheckRegistry();

        try {
            healthCheckRegistry.register("Tracking System", new ServiceHealthCheck(TrackingService.class));
        } catch (IllegalAccessException | IOException | InvocationTargetException e) {
            context.log("Error registering Tracking System health check", e);
        }
        // register here others healthchecks not managed with commons-rest
        healthCheckRegistry.register(RedisHealthCheck.NAME, new RedisHealthCheck());
        healthCheckRegistry.register(VisitConsumerHealthCheck.NAME, new VisitConsumerHealthCheck());
        healthCheckRegistry.register(ItineraryRatingConsumerHealthCheck.NAME, new ItineraryRatingConsumerHealthCheck());
        healthCheckRegistry.register(ShoppingItemAddedConsumerHealthCheck.NAME, new ShoppingItemAddedConsumerHealthCheck());

        context.setAttribute(HealthCheckServlet.REGISTRY_KEY, healthCheckRegistry);

        return healthCheckRegistry;
    }

    private Collection<Class> getConfigurations() {
        return new ArrayList<>();
    }

    private void initConfigurationEngine(HealthCheckRegistry healthCheckRegistry, DumpStateRegistry dumpStateRegistry) {
        AutoHealthCheckRegister healthRegister = new AutoHealthCheckRegister(healthCheckRegistry);
        AutoDumpStateRegister dumpRegister = new AutoDumpStateRegister(dumpStateRegistry);

        ConfigurationEngine.init(
                new ItineraryRatingFeatureHistoryServiceDefaultModule(healthRegister, dumpRegister),
                new OrikaModule(),
                new ItineraryLocationServiceCacheModule(healthRegister, dumpRegister));

        LOGGER.info("ConfigurationEngine has been initialized");
    }

    private void initLog4J() {
        try {
            ConfigurationFilesManager configurationFilesManager = new ConfigurationFilesManager();
            URL url = configurationFilesManager.getConfigurationFileUrl(LOG4J_FILENAME, this.getClass());
            URL fileRealURL = VFSUtils.getPhysicalURL(VFS.getChild(url.toURI()));
            PropertyConfigurator.configureAndWatch(fileRealURL.getFile(), LOG4J_WATCH_DELAY_MS);
            LOGGER.info("Log4j has been initialized with config file " + fileRealURL.getFile() + " and watch delay of " + (LOG4J_WATCH_DELAY_MS / 1000) + " seconds");

        } catch (IOException | URISyntaxException exp) {
            throw new IllegalStateException("Log4j cannot be initialized: file " + LOG4J_FILENAME + " cannot be load", exp);
        }
    }

    private void initMetrics() {
        MetricsManager.getInstance().addMetricsReporter(getAppName());
        MetricsManager.getInstance().changeReporterStatus(ReporterStatus.STARTED);
    }

    private static String getAppName() {
        try {
            return InitialContext.doLookup("java:app/AppName");
        } catch (NamingException e) {
            throw new IllegalStateException("cannot get AppName", e);
        }

    }

    private void launchConsumers() {
        launchModelEvaluationRequestedConsumer();
        launchVisitConsumer();
        launchShoppingItemAdded();
    }

    private void launchVisitConsumer() {
        visitConsumer = new KafkaConsumer<>(new JsonDeserializer<>(VisitMessage.class), "VISIT_INFORMATION_v1");
        visitConsumer.start(ConfigurationEngine.getInstance(VisitMessageProcessor.class), 8);
    }

    private void launchModelEvaluationRequestedConsumer() {
        modelEvaluationRequestedConsumer = new KafkaConsumer<>(new DomainEventDeserializer<>(ModelEvaluationRequested.getClassSchema()), "ITINERARY_RATING.REQUEST");
        modelEvaluationRequestedConsumer.start(ConfigurationEngine.getInstance(ModelEvaluationRequestedProcessor.class), 2);
    }

    private void launchShoppingItemAdded() {
        shoppingItemAddedConsumer = new KafkaConsumer<>(new DomainEventDeserializer<>(ShoppingItemAdded.getClassSchema()), "SHOPPING_CART.SHOPPING_ITEM");
        shoppingItemAddedConsumer.start(ConfigurationEngine.getInstance(ShoppingItemAddedProcessor.class), 4);
    }

}
