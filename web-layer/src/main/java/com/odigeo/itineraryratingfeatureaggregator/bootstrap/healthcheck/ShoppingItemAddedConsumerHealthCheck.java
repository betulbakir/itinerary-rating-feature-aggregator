package com.odigeo.itineraryratingfeatureaggregator.bootstrap.healthcheck;

import com.odigeo.commons.messaging.KafkaTopicHealthCheck;

public class ShoppingItemAddedConsumerHealthCheck extends KafkaTopicHealthCheck {

    public static final String NAME = "Kafka-consumer-shopping-item-added";

    private static final String TOPIC_NAME = "SHOPPING_CART.SHOPPING_ITEM";

    public ShoppingItemAddedConsumerHealthCheck() {
        super(TOPIC_NAME);
    }
}
