package com.odigeo.itineraryratingfeatureaggregator.bootstrap.dump;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.dump.DumpState;
import com.odigeo.itineraryratingfeatureaggregator.cache.VisitCache;
import com.odigeo.persistance.cache.impl.distributed.RedisDistributedCache;

import java.util.Map;
import java.util.TreeMap;

public class VisitCacheDump implements DumpState {

    @Override
    public String getName() {
        return "VisitCache";
    }

    @Override
    public Map<String, String> getProperties() {
        VisitCache cache = ConfigurationEngine.getInstance(VisitCache.class);
        Map<String, String> properties = new TreeMap<>();
        RedisDistributedCache redisCache = (RedisDistributedCache) cache.getCache();
        properties.put("class", redisCache.getClassName());
        properties.put("storageSpaceName", redisCache.getStorageSpaceName());
        properties.put("name", redisCache.getName());
        properties.put("hostName", redisCache.getHostname());
        properties.put("port", String.valueOf(redisCache.getPort()));
        properties.put("expirationTimeInSeconds", String.valueOf(redisCache.getExpirationTimeInSeconds()));
        return properties;
    }

}
