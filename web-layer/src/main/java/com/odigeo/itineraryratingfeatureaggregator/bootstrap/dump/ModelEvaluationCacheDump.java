package com.odigeo.itineraryratingfeatureaggregator.bootstrap.dump;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.dump.DumpState;
import com.odigeo.itineraryratingfeatureaggregator.cache.ModelEvaluationCache;
import com.odigeo.persistance.cache.impl.distributed.RedisDistributedCache;

import java.util.Map;
import java.util.TreeMap;

public class ModelEvaluationCacheDump implements DumpState {

    @Override
    public String getName() {
        return "ModelEvaluationCache";
    }

    @Override
    public Map<String, String> getProperties() {
        ModelEvaluationCache evaluationCache = ConfigurationEngine.getInstance(ModelEvaluationCache.class);
        Map<String, String> properties = new TreeMap<>();
        RedisDistributedCache cache = (RedisDistributedCache) evaluationCache.getCache();
        properties.put("class", cache.getClassName());
        properties.put("storageSpaceName", cache.getStorageSpaceName());
        properties.put("name", cache.getName());
        properties.put("hostName", cache.getHostname());
        properties.put("port", String.valueOf(cache.getPort()));
        properties.put("expirationTimeInSeconds", String.valueOf(cache.getExpirationTimeInSeconds()));
        return properties;
    }

}
