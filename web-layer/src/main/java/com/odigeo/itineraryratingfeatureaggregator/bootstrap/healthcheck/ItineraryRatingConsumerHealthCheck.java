package com.odigeo.itineraryratingfeatureaggregator.bootstrap.healthcheck;

import com.odigeo.commons.messaging.KafkaTopicHealthCheck;

public class ItineraryRatingConsumerHealthCheck extends KafkaTopicHealthCheck {

    public static final String NAME = "Kafka-consumer-itinerary-rating";

    private static final String TOPIC_NAME = "ITINERARY_RATING.REQUEST";

    public ItineraryRatingConsumerHealthCheck() {
        super(TOPIC_NAME);
    }
}
