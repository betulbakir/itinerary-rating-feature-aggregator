package com.odigeo.itineraryratingfeatureaggregator;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class InterfaceTest {

    public static final int RANDOM_INTERFACE_ID = -2;

    @Test
    public void testById() {
        Interface actualInterface = Interface.byId(Interface.ANDROID.getId());
        assertEquals(actualInterface, Interface.ANDROID);
    }
    @Test
    public void testByIdWithAnUnknownId() {
        Interface actualInterface = Interface.byId(RANDOM_INTERFACE_ID);
        assertEquals(actualInterface, Interface.UNKNOWN);
    }
}
