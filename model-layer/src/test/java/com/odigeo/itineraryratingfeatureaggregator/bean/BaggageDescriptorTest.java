package com.odigeo.itineraryratingfeatureaggregator.bean;

import com.odigeo.itineraryratingfeatureaggregator.PojoTestUtils;
import org.testng.annotations.Test;

public class BaggageDescriptorTest {

    @Test
    public void pojoTest() {
        PojoTestUtils.validateAccessors(BaggageDescriptor.class);
    }

}
