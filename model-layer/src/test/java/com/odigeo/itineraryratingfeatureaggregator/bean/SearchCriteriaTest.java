package com.odigeo.itineraryratingfeatureaggregator.bean;

import com.odigeo.itineraryratingfeatureaggregator.PojoTestUtils;
import org.testng.annotations.Test;

public class SearchCriteriaTest {

    @Test
    public void pojoTest() {
        PojoTestUtils.validateAccessors(SearchCriteria.class);
    }

}
