package com.odigeo.itineraryratingfeatureaggregator.bean;

import com.odigeo.itineraryratingfeatureaggregator.PojoTestUtils;
import org.testng.annotations.Test;

public class MoneyTest {

    @Test
    public void pojoTest() {
        PojoTestUtils.validateAccessors(Money.class);
    }

}
