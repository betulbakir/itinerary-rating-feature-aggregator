package com.odigeo.itineraryratingfeatureaggregator.bean;

import com.odigeo.itineraryratingfeatureaggregator.PojoTestUtils;
import org.testng.annotations.Test;

public class ItineraryTest {

    @Test
    public void pojoTest() {
        PojoTestUtils.validateAccessors(Itinerary.class);
    }
}
