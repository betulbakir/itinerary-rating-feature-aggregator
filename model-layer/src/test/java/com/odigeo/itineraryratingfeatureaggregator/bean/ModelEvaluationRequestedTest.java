package com.odigeo.itineraryratingfeatureaggregator.bean;

import com.odigeo.itineraryratingfeatureaggregator.PojoTestUtils;
import org.testng.annotations.Test;

public class ModelEvaluationRequestedTest {

    @Test
    public void pojoTest() {
        PojoTestUtils.validateAccessors(ModelEvaluationRequested.class);
    }

}
