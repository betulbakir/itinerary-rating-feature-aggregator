package com.odigeo.itineraryratingfeatureaggregator.bean;

import java.math.BigDecimal;
import java.util.List;
import java.util.StringJoiner;

public class Itinerary {

    private Money apparentPrice;
    private BigDecimal apparentPriceEur;
    private List<BaggageConditions> baggageConditionsAdultTraveler;
    private boolean hasFreeCancellation;
    private List<Segment> segments;

    public Money getApparentPrice() {
        return apparentPrice;
    }

    public void setApparentPrice(Money apparentPrice) {
        this.apparentPrice = apparentPrice;
    }

    public BigDecimal getApparentPriceEur() {
        return apparentPriceEur;
    }

    public void setApparentPriceEur(BigDecimal apparentPriceEur) {
        this.apparentPriceEur = apparentPriceEur;
    }

    public List<BaggageConditions> getBaggageConditionsAdultTraveler() {
        return baggageConditionsAdultTraveler;
    }

    public void setBaggageConditionsAdultTraveler(List<BaggageConditions> baggageConditionsAdultTraveler) {
        this.baggageConditionsAdultTraveler = baggageConditionsAdultTraveler;
    }

    public boolean isHasFreeCancellation() {
        return hasFreeCancellation;
    }

    public void setHasFreeCancellation(boolean hasFreeCancellation) {
        this.hasFreeCancellation = hasFreeCancellation;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Itinerary.class.getSimpleName() + "[", "]")
                .add("apparentPrice=" + apparentPrice)
                .add("apparentPriceEur=" + apparentPriceEur)
                .add("baggageConditionsAdultTraveler=" + baggageConditionsAdultTraveler)
                .add("hasFreeCancellation=" + hasFreeCancellation)
                .add("segments=" + segments)
                .toString();
    }
}
