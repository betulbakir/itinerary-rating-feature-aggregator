package com.odigeo.itineraryratingfeatureaggregator.bean;

import com.odigeo.itineraryratingfeatureaggregator.ZoneDateTimeHelper;

import java.time.Instant;
import java.util.StringJoiner;

public class Section {

    private String arrivalAirportIataCode;
    private String arrivalCityIataCode;
    private Instant arrivalTime;
    private String departureAirportIataCode;
    private String departureCityIataCode;
    private Instant departureTime;

    public String getArrivalAirportIataCode() {
        return arrivalAirportIataCode;
    }

    public void setArrivalAirportIataCode(String arrivalAirportIataCode) {
        this.arrivalAirportIataCode = arrivalAirportIataCode;
    }

    public String getArrivalCityIataCode() {
        return arrivalCityIataCode;
    }

    public void setArrivalCityIataCode(String arrivalCityIataCode) {
        this.arrivalCityIataCode = arrivalCityIataCode;
    }

    public Instant getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Instant arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureAirportIataCode() {
        return departureAirportIataCode;
    }

    public void setDepartureAirportIataCode(String departureAirportIataCode) {
        this.departureAirportIataCode = departureAirportIataCode;
    }

    public String getDepartureCityIataCode() {
        return departureCityIataCode;
    }

    public void setDepartureCityIataCode(String departureCityIataCode) {
        this.departureCityIataCode = departureCityIataCode;
    }

    public Instant getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Instant departureTime) {
        this.departureTime = departureTime;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Section.class.getSimpleName() + "[", "]")
                .add("arrivalAirportIataCode='" + arrivalAirportIataCode + "'")
                .add("arrivalCityIataCode='" + arrivalCityIataCode + "'")
                .add("arrivalTime=" + ZoneDateTimeHelper.DATE_FORMATTER.format(arrivalTime))
                .add("departureAirportIataCode='" + departureAirportIataCode + "'")
                .add("departureCityIataCode='" + departureCityIataCode + "'")
                .add("departureTime=" + ZoneDateTimeHelper.DATE_FORMATTER.format(departureTime))
                .toString();
    }
}
