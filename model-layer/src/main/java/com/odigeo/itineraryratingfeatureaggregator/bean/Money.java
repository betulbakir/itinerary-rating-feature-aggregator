package com.odigeo.itineraryratingfeatureaggregator.bean;

import java.math.BigDecimal;
import java.util.StringJoiner;

public class Money {

    private BigDecimal amount;
    private String currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Money.class.getSimpleName() + "[", "]")
                .add("amount=" + amount)
                .add("currency='" + currency + "'")
                .toString();
    }

}
