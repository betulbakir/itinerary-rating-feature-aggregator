package com.odigeo.itineraryratingfeatureaggregator.bean;

import java.util.StringJoiner;

public class BaggageConditions {

    private BaggageDescriptor baggageDescriptor;

    public BaggageDescriptor getBaggageDescriptor() {
        return baggageDescriptor;
    }

    public void setBaggageDescriptor(BaggageDescriptor baggageDescriptor) {
        this.baggageDescriptor = baggageDescriptor;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BaggageConditions.class.getSimpleName() + "[", "]")
                .add("baggageDescriptor=" + baggageDescriptor)
                .toString();
    }

}
