package com.odigeo.itineraryratingfeatureaggregator.bean;

import java.util.StringJoiner;

public class BaggageDescriptor {

    private int numPieces;
    private int numKilos;

    public int getNumPieces() {
        return numPieces;
    }

    public void setNumPieces(int numPieces) {
        this.numPieces = numPieces;
    }

    public int getNumKilos() {
        return numKilos;
    }

    public void setNumKilos(int numKilos) {
        this.numKilos = numKilos;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BaggageDescriptor.class.getSimpleName() + "[", "]")
                .add("numPieces=" + numPieces)
                .add("numKilos=" + numKilos)
                .toString();
    }
}
