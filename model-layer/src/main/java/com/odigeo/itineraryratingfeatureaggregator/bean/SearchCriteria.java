package com.odigeo.itineraryratingfeatureaggregator.bean;

import java.time.Instant;
import java.util.StringJoiner;

public class SearchCriteria {

    private int numAdults;
    private int numChildren;
    private Instant searchDate;
    private Instant departureDate;

    public int getNumAdults() {
        return numAdults;
    }

    public void setNumAdults(int numAdults) {
        this.numAdults = numAdults;
    }

    public int getNumChildren() {
        return numChildren;
    }

    public void setNumChildren(int numChildren) {
        this.numChildren = numChildren;
    }

    public Instant getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(Instant searchDate) {
        this.searchDate = searchDate;
    }

    public Instant getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Instant departureDate) {
        this.departureDate = departureDate;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SearchCriteria.class.getSimpleName() + "[", "]")
                .add("numAdults=" + numAdults)
                .add("numChildren=" + numChildren)
                .add("searchDate=" + searchDate)
                .add("departureDate=" + departureDate)
                .toString();
    }
}
