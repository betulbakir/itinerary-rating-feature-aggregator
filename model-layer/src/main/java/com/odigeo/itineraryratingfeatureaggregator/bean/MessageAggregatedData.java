package com.odigeo.itineraryratingfeatureaggregator.bean;


import java.time.Instant;

public class MessageAggregatedData {

    private ItineraryShoppingItem itineraryItemAdded;
    private ModelEvaluationRequested modelEvaluation;
    private String uniqueUserCookieId;
    private Instant itemAddedOccurrence;

    public MessageAggregatedData(ItineraryShoppingItem itineraryItemAdded, ModelEvaluationRequested modelEvaluation, String uniqueUserCookieId, Instant itemAddedOccurrence) {
        this.itineraryItemAdded = itineraryItemAdded;
        this.modelEvaluation = modelEvaluation;
        this.uniqueUserCookieId = uniqueUserCookieId;
        this.itemAddedOccurrence = itemAddedOccurrence;
    }

    public ItineraryShoppingItem getItineraryItemAdded() {
        return itineraryItemAdded;
    }

    public void setItineraryItemAdded(ItineraryShoppingItem itineraryItemAdded) {
        this.itineraryItemAdded = itineraryItemAdded;
    }

    public ModelEvaluationRequested getModelEvaluation() {
        return modelEvaluation;
    }

    public void setModelEvaluation(ModelEvaluationRequested modelEvaluation) {
        this.modelEvaluation = modelEvaluation;
    }

    public String getUniqueUserCookieId() {
        return uniqueUserCookieId;
    }

    public void setUniqueUserCookieId(String uniqueUserCookieId) {
        this.uniqueUserCookieId = uniqueUserCookieId;
    }

    public Instant getItemAddedOccurrence() {
        return itemAddedOccurrence;
    }

    public void setItemAddedOccurrence(Instant itemAddedOccurrence) {
        this.itemAddedOccurrence = itemAddedOccurrence;
    }

    @Override
    public String toString() {
        return "MessageAggregatedData{"
                + "itemAdded=" + itineraryItemAdded
                + ", modelEvaluation=" + modelEvaluation
                + ", uniqueUserCookieId='" + uniqueUserCookieId
                + '\''
                + ", itemAddedOccurrence=" + itemAddedOccurrence
                + '}';
    }
}
