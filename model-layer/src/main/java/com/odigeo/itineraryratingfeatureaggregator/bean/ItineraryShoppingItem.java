package com.odigeo.itineraryratingfeatureaggregator.bean;

import java.util.StringJoiner;

public class ItineraryShoppingItem {

    private long id;
    private long searchId;
    private long visitId;
    private Itinerary itinerary;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSearchId() {
        return searchId;
    }

    public void setSearchId(long searchId) {
        this.searchId = searchId;
    }

    public long getVisitId() {
        return visitId;
    }

    public void setVisitId(long visitId) {
        this.visitId = visitId;
    }

    public Itinerary getItinerary() {
        return itinerary;
    }

    public void setItinerary(Itinerary itinerary) {
        this.itinerary = itinerary;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ItineraryShoppingItem.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("searchId=" + searchId)
                .add("visitId=" + visitId)
                .add("itinerary=" + itinerary)
                .toString();
    }

}
