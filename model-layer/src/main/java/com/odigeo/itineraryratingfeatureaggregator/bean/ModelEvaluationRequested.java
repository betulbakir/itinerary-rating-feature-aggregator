package com.odigeo.itineraryratingfeatureaggregator.bean;

import java.util.StringJoiner;

public class ModelEvaluationRequested {

    private long searchId;
    private long visitId;
    private SearchCriteria searchCriteria;
    private SearchResultsStatistics searchResultsStatistics;

    public long getSearchId() {
        return searchId;
    }

    public void setSearchId(long searchId) {
        this.searchId = searchId;
    }

    public long getVisitId() {
        return visitId;
    }

    public void setVisitId(long visitId) {
        this.visitId = visitId;
    }

    public SearchCriteria getSearchCriteria() {
        return searchCriteria;
    }

    public void setSearchCriteria(SearchCriteria searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    public SearchResultsStatistics getSearchResultsStatistics() {
        return searchResultsStatistics;
    }

    public void setSearchResultsStatistics(SearchResultsStatistics searchResultsStatistics) {
        this.searchResultsStatistics = searchResultsStatistics;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ModelEvaluationRequested.class.getSimpleName() + "[", "]")
                .add("searchId=" + searchId)
                .add("visitId=" + visitId)
                .add("searchCriteria=" + searchCriteria)
                .add("searchResultsStatistics=" + searchResultsStatistics)
                .toString();
    }
}
