package com.odigeo.itineraryratingfeatureaggregator.bean;

import java.math.BigDecimal;
import java.util.StringJoiner;

public class SearchResultsStatistics {

    private BigDecimal maxPrice;
    private BigDecimal minPrice;
    private Long maxStayTimeSeconds;
    private Long minStayTimeSeconds;
    private Long maxCityToAirportTripDuration;
    private Long minCityToAirportTripDuration;
    private Long maxOutboundCityToAirportTripDuration;
    private Long minOutboundCityToAirportTripDuration;
    private Long maxInboundCityToAirportTripDuration;
    private Long minInboundCityToAirportTripDuration;
    private Long maxFlightDuration;
    private Long minFlightDuration;
    private Long maxOutboundFlightDuration;
    private Long minOutboundFlightDuration;
    private Long maxInboundFlightDuration;
    private Long minInboundFlightDuration;
    private Long maxStopoverDuration;
    private Long minStopoverDuration;
    private Long maxOutboundStopoverDuration;
    private Long minOutboundStopoverDuration;
    private Long maxInboundStopoverDuration;
    private Long minInboundStopoverDuration;
    private Long maxNumberStopovers;
    private Long minNumberStopovers;
    private Long maxOutboundNumberStopovers;
    private Long minOutboundNumberStopovers;
    private Long maxInboundNumberStopovers;
    private Long minInboundNumberStopovers;
    private Long maxNumberAvailableSeats;
    private Long minNumberAvailableSeats;
    private Long maxOutboundNumberAvailableSeats;
    private Long minOutboundNumberAvailableSeats;
    private Long maxInboundNumberAvailableSeats;
    private Long minInboundNumberAvailableSeats;

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public Long getMaxStayTimeSeconds() {
        return maxStayTimeSeconds;
    }

    public void setMaxStayTimeSeconds(Long maxStayTimeSeconds) {
        this.maxStayTimeSeconds = maxStayTimeSeconds;
    }

    public Long getMinStayTimeSeconds() {
        return minStayTimeSeconds;
    }

    public void setMinStayTimeSeconds(Long minStayTimeSeconds) {
        this.minStayTimeSeconds = minStayTimeSeconds;
    }

    public Long getMaxCityToAirportTripDuration() {
        return maxCityToAirportTripDuration;
    }

    public void setMaxCityToAirportTripDuration(Long maxCityToAirportTripDuration) {
        this.maxCityToAirportTripDuration = maxCityToAirportTripDuration;
    }

    public Long getMinCityToAirportTripDuration() {
        return minCityToAirportTripDuration;
    }

    public void setMinCityToAirportTripDuration(Long minCityToAirportTripDuration) {
        this.minCityToAirportTripDuration = minCityToAirportTripDuration;
    }

    public Long getMaxOutboundCityToAirportTripDuration() {
        return maxOutboundCityToAirportTripDuration;
    }

    public void setMaxOutboundCityToAirportTripDuration(Long maxOutboundCityToAirportTripDuration) {
        this.maxOutboundCityToAirportTripDuration = maxOutboundCityToAirportTripDuration;
    }

    public Long getMinOutboundCityToAirportTripDuration() {
        return minOutboundCityToAirportTripDuration;
    }

    public void setMinOutboundCityToAirportTripDuration(Long minOutboundCityToAirportTripDuration) {
        this.minOutboundCityToAirportTripDuration = minOutboundCityToAirportTripDuration;
    }

    public Long getMaxInboundCityToAirportTripDuration() {
        return maxInboundCityToAirportTripDuration;
    }

    public void setMaxInboundCityToAirportTripDuration(Long maxInboundCityToAirportTripDuration) {
        this.maxInboundCityToAirportTripDuration = maxInboundCityToAirportTripDuration;
    }

    public Long getMinInboundCityToAirportTripDuration() {
        return minInboundCityToAirportTripDuration;
    }

    public void setMinInboundCityToAirportTripDuration(Long minInboundCityToAirportTripDuration) {
        this.minInboundCityToAirportTripDuration = minInboundCityToAirportTripDuration;
    }

    public Long getMaxFlightDuration() {
        return maxFlightDuration;
    }

    public void setMaxFlightDuration(Long maxFlightDuration) {
        this.maxFlightDuration = maxFlightDuration;
    }

    public Long getMinFlightDuration() {
        return minFlightDuration;
    }

    public void setMinFlightDuration(Long minFlightDuration) {
        this.minFlightDuration = minFlightDuration;
    }

    public Long getMaxOutboundFlightDuration() {
        return maxOutboundFlightDuration;
    }

    public void setMaxOutboundFlightDuration(Long maxOutboundFlightDuration) {
        this.maxOutboundFlightDuration = maxOutboundFlightDuration;
    }

    public Long getMinOutboundFlightDuration() {
        return minOutboundFlightDuration;
    }

    public void setMinOutboundFlightDuration(Long minOutboundFlightDuration) {
        this.minOutboundFlightDuration = minOutboundFlightDuration;
    }

    public Long getMaxInboundFlightDuration() {
        return maxInboundFlightDuration;
    }

    public void setMaxInboundFlightDuration(Long maxInboundFlightDuration) {
        this.maxInboundFlightDuration = maxInboundFlightDuration;
    }

    public Long getMinInboundFlightDuration() {
        return minInboundFlightDuration;
    }

    public void setMinInboundFlightDuration(Long minInboundFlightDuration) {
        this.minInboundFlightDuration = minInboundFlightDuration;
    }

    public Long getMaxStopoverDuration() {
        return maxStopoverDuration;
    }

    public void setMaxStopoverDuration(Long maxStopoverDuration) {
        this.maxStopoverDuration = maxStopoverDuration;
    }

    public Long getMinStopoverDuration() {
        return minStopoverDuration;
    }

    public void setMinStopoverDuration(Long minStopoverDuration) {
        this.minStopoverDuration = minStopoverDuration;
    }

    public Long getMaxOutboundStopoverDuration() {
        return maxOutboundStopoverDuration;
    }

    public void setMaxOutboundStopoverDuration(Long maxOutboundStopoverDuration) {
        this.maxOutboundStopoverDuration = maxOutboundStopoverDuration;
    }

    public Long getMinOutboundStopoverDuration() {
        return minOutboundStopoverDuration;
    }

    public void setMinOutboundStopoverDuration(Long minOutboundStopoverDuration) {
        this.minOutboundStopoverDuration = minOutboundStopoverDuration;
    }

    public Long getMaxInboundStopoverDuration() {
        return maxInboundStopoverDuration;
    }

    public void setMaxInboundStopoverDuration(Long maxInboundStopoverDuration) {
        this.maxInboundStopoverDuration = maxInboundStopoverDuration;
    }

    public Long getMinInboundStopoverDuration() {
        return minInboundStopoverDuration;
    }

    public void setMinInboundStopoverDuration(Long minInboundStopoverDuration) {
        this.minInboundStopoverDuration = minInboundStopoverDuration;
    }

    public Long getMaxNumberStopovers() {
        return maxNumberStopovers;
    }

    public void setMaxNumberStopovers(Long maxNumberStopovers) {
        this.maxNumberStopovers = maxNumberStopovers;
    }

    public Long getMinNumberStopovers() {
        return minNumberStopovers;
    }

    public void setMinNumberStopovers(Long minNumberStopovers) {
        this.minNumberStopovers = minNumberStopovers;
    }

    public Long getMaxOutboundNumberStopovers() {
        return maxOutboundNumberStopovers;
    }

    public void setMaxOutboundNumberStopovers(Long maxOutboundNumberStopovers) {
        this.maxOutboundNumberStopovers = maxOutboundNumberStopovers;
    }

    public Long getMinOutboundNumberStopovers() {
        return minOutboundNumberStopovers;
    }

    public void setMinOutboundNumberStopovers(Long minOutboundNumberStopovers) {
        this.minOutboundNumberStopovers = minOutboundNumberStopovers;
    }

    public Long getMaxInboundNumberStopovers() {
        return maxInboundNumberStopovers;
    }

    public void setMaxInboundNumberStopovers(Long maxInboundNumberStopovers) {
        this.maxInboundNumberStopovers = maxInboundNumberStopovers;
    }

    public Long getMinInboundNumberStopovers() {
        return minInboundNumberStopovers;
    }

    public void setMinInboundNumberStopovers(Long minInboundNumberStopovers) {
        this.minInboundNumberStopovers = minInboundNumberStopovers;
    }

    public Long getMaxNumberAvailableSeats() {
        return maxNumberAvailableSeats;
    }

    public void setMaxNumberAvailableSeats(Long maxNumberAvailableSeats) {
        this.maxNumberAvailableSeats = maxNumberAvailableSeats;
    }

    public Long getMinNumberAvailableSeats() {
        return minNumberAvailableSeats;
    }

    public void setMinNumberAvailableSeats(Long minNumberAvailableSeats) {
        this.minNumberAvailableSeats = minNumberAvailableSeats;
    }

    public Long getMaxOutboundNumberAvailableSeats() {
        return maxOutboundNumberAvailableSeats;
    }

    public void setMaxOutboundNumberAvailableSeats(Long maxOutboundNumberAvailableSeats) {
        this.maxOutboundNumberAvailableSeats = maxOutboundNumberAvailableSeats;
    }

    public Long getMinOutboundNumberAvailableSeats() {
        return minOutboundNumberAvailableSeats;
    }

    public void setMinOutboundNumberAvailableSeats(Long minOutboundNumberAvailableSeats) {
        this.minOutboundNumberAvailableSeats = minOutboundNumberAvailableSeats;
    }

    public Long getMaxInboundNumberAvailableSeats() {
        return maxInboundNumberAvailableSeats;
    }

    public void setMaxInboundNumberAvailableSeats(Long maxInboundNumberAvailableSeats) {
        this.maxInboundNumberAvailableSeats = maxInboundNumberAvailableSeats;
    }

    public Long getMinInboundNumberAvailableSeats() {
        return minInboundNumberAvailableSeats;
    }

    public void setMinInboundNumberAvailableSeats(Long minInboundNumberAvailableSeats) {
        this.minInboundNumberAvailableSeats = minInboundNumberAvailableSeats;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SearchResultsStatistics.class.getSimpleName() + "[", "]")
                .add("maxPrice=" + maxPrice)
                .add("minPrice=" + minPrice)
                .add("maxStayTimeSeconds=" + maxStayTimeSeconds)
                .add("minStayTimeSeconds=" + minStayTimeSeconds)
                .add("maxCityToAirportTripDuration=" + maxCityToAirportTripDuration)
                .add("minCityToAirportTripDuration=" + minCityToAirportTripDuration)
                .add("maxOutboundCityToAirportTripDuration=" + maxOutboundCityToAirportTripDuration)
                .add("minOutboundCityToAirportTripDuration=" + minOutboundCityToAirportTripDuration)
                .add("maxInboundCityToAirportTripDuration=" + maxInboundCityToAirportTripDuration)
                .add("minInboundCityToAirportTripDuration=" + minInboundCityToAirportTripDuration)
                .add("maxFlightDuration=" + maxFlightDuration)
                .add("minFlightDuration=" + minFlightDuration)
                .add("maxOutboundFlightDuration=" + maxOutboundFlightDuration)
                .add("minOutboundFlightDuration=" + minOutboundFlightDuration)
                .add("maxInboundFlightDuration=" + maxInboundFlightDuration)
                .add("minInboundFlightDuration=" + minInboundFlightDuration)
                .add("maxStopoverDuration=" + maxStopoverDuration)
                .add("minStopoverDuration=" + minStopoverDuration)
                .add("maxOutboundStopoverDuration=" + maxOutboundStopoverDuration)
                .add("minOutboundStopoverDuration=" + minOutboundStopoverDuration)
                .add("maxInboundStopoverDuration=" + maxInboundStopoverDuration)
                .add("minInboundStopoverDuration=" + minInboundStopoverDuration)
                .add("maxNumberStopovers=" + maxNumberStopovers)
                .add("minNumberStopovers=" + minNumberStopovers)
                .add("maxOutboundNumberStopovers=" + maxOutboundNumberStopovers)
                .add("minOutboundNumberStopovers=" + minOutboundNumberStopovers)
                .add("maxInboundNumberStopovers=" + maxInboundNumberStopovers)
                .add("minInboundNumberStopovers=" + minInboundNumberStopovers)
                .add("maxNumberAvailableSeats=" + maxNumberAvailableSeats)
                .add("minNumberAvailableSeats=" + minNumberAvailableSeats)
                .add("maxOutboundNumberAvailableSeats=" + maxOutboundNumberAvailableSeats)
                .add("minOutboundNumberAvailableSeats=" + minOutboundNumberAvailableSeats)
                .add("maxInboundNumberAvailableSeats=" + maxInboundNumberAvailableSeats)
                .add("minInboundNumberAvailableSeats=" + minInboundNumberAvailableSeats)
                .toString();
    }
}
