package com.odigeo.itineraryratingfeatureaggregator.bean;

import com.odigeo.itineraryratingfeatureaggregator.ZoneDateTimeHelper;

import java.time.Instant;
import java.util.List;
import java.util.StringJoiner;

public class Segment {

    private String departureAirportIataCode;
    private String departureCityIataCode;
    private Instant departureTime;
    private String arrivalAirportIataCode;
    private String arrivalCityIataCode;
    private Instant arrivalTime;
    private long duration;
    private String marketingCarrierCode;
    private int numStopovers;
    private int seatsAvailable;
    private List<BaggageConditions> baggageConditionsAdultTraveler;
    private List<Section> sections;

    public String getDepartureAirportIataCode() {
        return departureAirportIataCode;
    }

    public void setDepartureAirportIataCode(String departureAirportIataCode) {
        this.departureAirportIataCode = departureAirportIataCode;
    }

    public String getDepartureCityIataCode() {
        return departureCityIataCode;
    }

    public void setDepartureCityIataCode(String departureCityIataCode) {
        this.departureCityIataCode = departureCityIataCode;
    }

    public Instant getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Instant departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalAirportIataCode() {
        return arrivalAirportIataCode;
    }

    public void setArrivalAirportIataCode(String arrivalAirportIataCode) {
        this.arrivalAirportIataCode = arrivalAirportIataCode;
    }

    public String getArrivalCityIataCode() {
        return arrivalCityIataCode;
    }

    public void setArrivalCityIataCode(String arrivalCityIataCode) {
        this.arrivalCityIataCode = arrivalCityIataCode;
    }

    public Instant getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Instant arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getMarketingCarrierCode() {
        return marketingCarrierCode;
    }

    public void setMarketingCarrierCode(String marketingCarrierCode) {
        this.marketingCarrierCode = marketingCarrierCode;
    }

    public int getNumStopovers() {
        return numStopovers;
    }

    public void setNumStopovers(int numStopovers) {
        this.numStopovers = numStopovers;
    }

    public int getSeatsAvailable() {
        return seatsAvailable;
    }

    public void setSeatsAvailable(int seatsAvailable) {
        this.seatsAvailable = seatsAvailable;
    }

    public List<BaggageConditions> getBaggageConditionsAdultTraveler() {
        return baggageConditionsAdultTraveler;
    }

    public void setBaggageConditionsAdultTraveler(List<BaggageConditions> baggageConditionsAdultTraveler) {
        this.baggageConditionsAdultTraveler = baggageConditionsAdultTraveler;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Segment.class.getSimpleName() + "[", "]")
                .add("arrivalAirportIataCode='" + arrivalAirportIataCode + "'")
                .add("arrivalCityIataCode='" + arrivalCityIataCode + "'")
                .add("arrivalTime=" + ZoneDateTimeHelper.DATE_FORMATTER.format(arrivalTime))
                .add("departureAirportIataCode='" + departureAirportIataCode + "'")
                .add("departureCityIataCode='" + departureCityIataCode + "'")
                .add("departureTime=" + ZoneDateTimeHelper.DATE_FORMATTER.format(departureTime))
                .add("duration=" + duration)
                .add("marketingCarrierCode='" + marketingCarrierCode + "'")
                .add("numStopovers=" + numStopovers)
                .add("seatsAvailable=" + seatsAvailable)
                .add("baggageConditionsAdultTraveler=" + baggageConditionsAdultTraveler)
                .add("sections=" + sections)
                .toString();
    }
}
