package com.odigeo.itineraryratingfeatureaggregator;

public enum Interface {

    NATIVE_ANDROID_TRAVELLINK(31),
    NATIVE_IOS_TRAVELLINK(30),
    NATIVE_IPAD_GOV(27),
    NATIVE_IPAD_OPODO(26),
    NATIVE_IPAD_EDREAMS(25),
    NATIVE_ANDROID_EDREAMS(24),
    NATIVE_ANDROID_OPODO(23),
    NATIVE_ANDROID_GOV(22),
    ONE_FRONT_SMARTPHONE(17),
    ONE_FRONT_DESKTOP(16),
    NATIVE_IOS_GOV(12),
    NATIVE_IOS_OPODO(11),
    ANDROID(9),
    NATIVE_IOS_EDREAMS(8),
    UNKNOWN(0);

    private final int id;

    Interface(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static Interface byId(int interfaceId) {
        for (Interface anInterface : Interface.values()) {
            if (anInterface.getId() == interfaceId) {
                return anInterface;
            }
        }
        return UNKNOWN;
    }
}
