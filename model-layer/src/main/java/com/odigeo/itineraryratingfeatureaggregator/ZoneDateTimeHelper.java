package com.odigeo.itineraryratingfeatureaggregator;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class ZoneDateTimeHelper {
    public static final ZoneId UTC = ZoneId.of("UTC");

    public static final DateTimeFormatter DATE_FORMATTER =
            DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG)
                    .withLocale(new Locale("es", "ES"))
                    .withZone(UTC);

}
