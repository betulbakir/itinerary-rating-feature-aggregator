package com.odigeo.itineraryratingfeatureaggregator.cache;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class CacheKeyTest {

    private static final long LONG = 10L;

    @Test
    public void testGetStringKey() {
        CacheKey actualCacheKey = new CacheKey(CacheKeyType.SEARCH_ID, LONG);
        assertEquals(actualCacheKey.getStringKey(), "search.id:10");
    }

    @Test
    public void testEquals() {
        CacheKey key = new CacheKey(CacheKeyType.SEARCH_ID, LONG);
        CacheKey key2 = new CacheKey(CacheKeyType.SEARCH_ID, LONG);
        assertTrue(key.equals(key2));
    }

    @Test
    public void testEqualsSameObject() {
        CacheKey key = new CacheKey(CacheKeyType.SEARCH_ID, LONG);
        assertTrue(key.equals(key));
    }

    @Test
    public void testEqualsWithNullValue() {
        CacheKey key = new CacheKey(CacheKeyType.SEARCH_ID, LONG);
        CacheKey key2 = null;
        assertFalse(key.equals(key2));
    }

    @Test
    public void testEqualsDifferentClasses() {
        CacheKey key = new CacheKey(CacheKeyType.SEARCH_ID, LONG);
        String stringKey = "StringKey";
        assertFalse(key.equals(stringKey));
    }

    @Test
    public void testHashCode() {
        CacheKey key = new CacheKey(CacheKeyType.SEARCH_ID, LONG);
        CacheKey key2 = new CacheKey(CacheKeyType.SEARCH_ID, LONG);
        assertTrue(key.hashCode() == key2.hashCode());
    }

}
