package com.odigeo.itineraryratingfeatureaggregator.cache;

import com.edreams.configuration.ConfigurationEngine;
import com.edreams.persistance.cache.Cache;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class ModelEvaluationCacheTest {

    @Mock
    private Cache cacheMock;

    private ModelEvaluationCache cache = new ModelEvaluationCache();

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init();
        cache.setCache(cacheMock);
    }

    @Test
    public void testAddEntry() {
        CacheKey key = new CacheKey(CacheKeyType.SEARCH_ID, 1);
        String value = "value";
        cache.addEntry(key, value);
        verify(cacheMock).addEntry(key.getStringKey(), value);
    }

    @Test
    public void testFetch() {
        CacheKey key = new CacheKey(CacheKeyType.SEARCH_ID, 1);
        String value = "value";

        when(cacheMock.fetchValue(key.getStringKey())).thenReturn(value);

        String actualValue = (String) cache.fetchValue(key);

        assertEquals(actualValue, value);
    }

}
