package com.odigeo.itineraryratingfeatureaggregator.cache;

public enum CacheKeyType {
    SEARCH_ID("search.id"), VISIT_ID("visit.id");

    private String keyId;

    CacheKeyType(String keyId) {
        this.keyId = keyId;
    }

    public String getKeyId() {
        return keyId;
    }
}
