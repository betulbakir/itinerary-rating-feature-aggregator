package com.odigeo.itineraryratingfeatureaggregator.cache;

import java.util.Objects;

public class CacheKey {

    private final CacheKeyType type;
    private final long value;

    public CacheKey(CacheKeyType type, long value) {
        this.type = type;
        this.value = value;
    }

    public String getStringKey() {
        return type.getKeyId() + ":" + value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CacheKey cacheKey = (CacheKey) o;
        return value == cacheKey.value
                && type == cacheKey.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, value);
    }
}
