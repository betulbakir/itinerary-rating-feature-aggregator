package com.odigeo.itineraryratingfeatureaggregator.cache;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.edreams.persistance.cache.Cache;
import com.google.inject.Singleton;

import java.io.Serializable;

@Singleton
@ConfiguredInPropertiesFile
public class ModelEvaluationCache {

    private Cache cache;

    public void setCache(Cache cache) {
        this.cache = cache;
    }

    public Cache getCache() {
        return cache;
    }

    public void addEntry(CacheKey key, Serializable serializable) {
        cache.addEntry(key.getStringKey(), serializable);
    }

    public Serializable fetchValue(CacheKey key) {
        return cache.fetchValue(key.getStringKey());
    }

}
