# QA Test environment

**1-** Use provides doc/docker-compose.xml file in order to start local environment for test. This configuration will start locally:

- IRFA (port:8080)
- Site (port:8081)
- MIR (port:8082)
- Redis cache (port:6379)

**2-** Run a search with the Aggregate flow in the provide soap project (PERSONALIZATION_FLOWS.xml) , the project should call visit, mir and site and the messages of all of these modules should be populated in qa kafka. 

**3-** Testing Happy path:  

In aggregator logs, after doing the search you should see the visit message inserted (processed by VisitMessageProcessor), the mir message inserted (processed by ModelEvaluationRequestedProcessor) and finally if everything has been inserted properly you should see the information from the MessageAggregatedData processed in ShoppingItemAddedProcessor. MessageAggregatedData stores information of the 3 messages. 

Validate that the info logged by ShoppingItemAddedProcessor is the same as you can see for VisitMessageProcessor and ModelEvaluationRequestedProcessor values (UniqueUserCookieId, visitId, searchId, prices, etc).

You can use to check logs this command 

`tail -500f ~/logsDocker/itinerary-rating-feature-aggregator/app.log | grep -E  'VisitMessageProcessor|ModelEvaluationRequestedProcessor|ShoppingItemAddedProcessor'`

**4-** Regarding metrics:

You should see that the counter "message_consumed_item_added" increased every time there is a new message consumed. 

You should see that the counter "message_processed_item_added" increased every time there is a new message processed. We consider that the message has been processed when we have found the UniqueUserCookieId in the cache and the ModelEvaluation information. In any other case, we should see that the counter "message_consumed_item_added" increases but the "message_processed_item_added" doesn't. 

