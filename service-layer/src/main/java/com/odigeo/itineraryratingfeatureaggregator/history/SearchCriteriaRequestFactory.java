package com.odigeo.itineraryratingfeatureaggregator.history;

import com.google.inject.Singleton;
import com.odigeo.itineraryratingfeatureaggregator.bean.SearchCriteria;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.SearchCriteriaRequest;

import static com.odigeo.itineraryratingfeatureaggregator.ZoneDateTimeHelper.UTC;

@Singleton
public class SearchCriteriaRequestFactory {

    public SearchCriteriaRequest getRequest(SearchCriteria criteria, Long searchId) {
        return new SearchCriteriaRequest.Builder()
                .withSearchId(searchId)
                .withDepartureDateUTC(criteria.getDepartureDate().atZone(UTC))
                .withSearchDateUTC(criteria.getSearchDate().atZone(UTC))
                .withNumberOfAdults(criteria.getNumAdults())
                .withNumberOfChildren(criteria.getNumChildren())
                .build();
    }
}
