package com.odigeo.itineraryratingfeatureaggregator.history;

import com.google.inject.Inject;
import com.odigeo.geoapi.v4.responses.Continent;
import com.odigeo.geoapi.v4.responses.Country;
import com.odigeo.geoapi.v4.responses.ItineraryLocation;
import com.odigeo.itineraryratingfeatureaggregator.bean.Segment;
import com.odigeo.itineraryratingfeatureaggregator.geoservice.GeoServiceHandler;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.CoordinatesRequest;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.GeographicalInformationRequest;

import java.util.Optional;

public class GeoInfoRequestFactory {

    private final GeoServiceHandler serviceHandler;

    @Inject
    public GeoInfoRequestFactory(GeoServiceHandler serviceHandler) {
        this.serviceHandler = serviceHandler;
    }

    public GeographicalInformationRequest getRequest(Segment segment) {
        Optional<ItineraryLocation> depLocation = getCoses(segment.getDepartureAirportIataCode());
        Optional<ItineraryLocation> arrLocation = getCoses(segment.getArrivalAirportIataCode());
        return new GeographicalInformationRequest.Builder()
                .withDepartureAirport(segment.getDepartureAirportIataCode())
                .withDepartureCity(segment.getDepartureCityIataCode())
                .withDepartureCountry(depLocation.map(location -> getCountry(location).getCountryCode()).orElse(null))
                .withDepartureContinentId((depLocation.map(location -> Integer.toString(getContinent(location).getContinentId())).orElse(null)))
                .withDepartureCoordinates(depLocation.map(this::getCoordinatesRequest).orElse(null))
                .withArrivalAirport(segment.getArrivalAirportIataCode())
                .withArrivalCity(segment.getArrivalCityIataCode())
                .withArrivalCountry(arrLocation.map(location -> getCountry(location).getCountryCode()).orElse(null))
                .withArrivalContinentId(arrLocation.map(location -> Integer.toString(getContinent(location).getContinentId())).orElse(null))
                .withArrivalCoordinates(arrLocation.map(this::getCoordinatesRequest).orElse(null))
                .build();
    }

    private Continent getContinent(ItineraryLocation location) {
        return getCountry(location).getContinent();
    }

    private Country getCountry(ItineraryLocation location) {
        return location.getCity().getCountry();
    }

    private Optional<ItineraryLocation> getCoses(String departureAirportIataCode) {
        return serviceHandler.getLocation(departureAirportIataCode);
    }

    private CoordinatesRequest getCoordinatesRequest(ItineraryLocation location) {
        return new CoordinatesRequest.Builder()
                .withLongitude(location.getCoordinates().getLongitude())
                .withLatitude(location.getCoordinates().getLatitude())
                .build();
    }
}
