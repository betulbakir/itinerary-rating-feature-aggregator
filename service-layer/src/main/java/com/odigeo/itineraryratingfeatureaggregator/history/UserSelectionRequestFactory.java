package com.odigeo.itineraryratingfeatureaggregator.history;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.itineraryratingfeatureaggregator.bean.MessageAggregatedData;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.UserSelectionRequest;

import static com.odigeo.itineraryratingfeatureaggregator.ZoneDateTimeHelper.UTC;

@Singleton
public class UserSelectionRequestFactory {

    private final SearchCriteriaRequestFactory searchCriteriaRequestFactory;
    private final ItinerarySelectedRequestFactory itinerarySelectedRequestFactory;
    private final ModelStatisticsRequestFactory modelStatisticsRequestFactory;

    @Inject
    public UserSelectionRequestFactory(SearchCriteriaRequestFactory searchCriteriaRequestFactory,
                                       ItinerarySelectedRequestFactory itinerarySelectedRequestFactory,
                                       ModelStatisticsRequestFactory modelStatisticsRequestFactory) {
        this.searchCriteriaRequestFactory = searchCriteriaRequestFactory;
        this.itinerarySelectedRequestFactory = itinerarySelectedRequestFactory;
        this.modelStatisticsRequestFactory = modelStatisticsRequestFactory;
    }

    public UserSelectionRequest getRequest(MessageAggregatedData data) {
        return new UserSelectionRequest.Builder()
                .withUserDevice(data.getUniqueUserCookieId())
                .withSelectionDateUTC(data.getItemAddedOccurrence().atZone(UTC))
                .withSearchCriteriaRequest(searchCriteriaRequestFactory.getRequest(data.getModelEvaluation().getSearchCriteria(), data.getItineraryItemAdded().getSearchId()))
                .withItinerarySelectedRequest(itinerarySelectedRequestFactory.getRequest(data.getItineraryItemAdded()))
                .withModelStatistics(modelStatisticsRequestFactory.getRequest(data.getModelEvaluation()))
                .build();
    }
}
