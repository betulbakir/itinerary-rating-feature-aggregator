package com.odigeo.itineraryratingfeatureaggregator.history;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.itineraryratingfeatureaggregator.bean.BaggageConditions;
import com.odigeo.itineraryratingfeatureaggregator.bean.Segment;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.SegmentRequest;

import java.util.List;

import static com.odigeo.itineraryratingfeatureaggregator.ZoneDateTimeHelper.UTC;

@Singleton
public class SegmentSelectedRequestFactory {

    private final BaggageSelectedRequestFactory baggageRequestFactory;
    private final GeoInfoRequestFactory geoInfoRequestFactory;
    private final StopoverDurationCalculator stopoverDurationCalculator;

    @Inject
    public SegmentSelectedRequestFactory(BaggageSelectedRequestFactory baggageRequestFactory, GeoInfoRequestFactory geoInfoRequestFactory,
                                         StopoverDurationCalculator stopoverDurationCalculator) {
        this.baggageRequestFactory = baggageRequestFactory;
        this.geoInfoRequestFactory = geoInfoRequestFactory;
        this.stopoverDurationCalculator = stopoverDurationCalculator;
    }

    public SegmentRequest getRequest(Segment segment, List<BaggageConditions> itineraryConditions) {
        return new SegmentRequest.Builder()
                .withDepartureDateUTC(segment.getDepartureTime().atZone(UTC))
                .withArrivalDateUTC(segment.getArrivalTime().atZone(UTC))
                .withNumStopovers(segment.getNumStopovers())
                .withMarketingCarrierCode(segment.getMarketingCarrierCode())
                .withSeatsAvailable(segment.getSeatsAvailable())
                .withBaggageIncludedType(baggageRequestFactory.getRequest(segment.getBaggageConditionsAdultTraveler(), itineraryConditions))
                .withGeographicalInformationRequest(geoInfoRequestFactory.getRequest(segment))
                .withDurStopovers(stopoverDurationCalculator.getStopoverDuration(segment))
                .withFlightDuration(segment.getDuration())
                .build();
    }

}
