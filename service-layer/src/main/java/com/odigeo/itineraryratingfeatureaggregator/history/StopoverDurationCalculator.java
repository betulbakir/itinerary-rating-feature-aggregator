package com.odigeo.itineraryratingfeatureaggregator.history;

import com.odigeo.itineraryratingfeatureaggregator.bean.Section;
import com.odigeo.itineraryratingfeatureaggregator.bean.Segment;

import java.time.Duration;

public class StopoverDurationCalculator {

    public long getStopoverDuration(Segment segment) {
        if (segment.getNumStopovers() > 0) {
            long durationStopovers = 0L;
            Section previousSection = null;
            for (Section section : segment.getSections()) {
                if (previousSection != null) {
                    if (previousSection.getArrivalTime() != null && section.getDepartureTime() != null) {
                        durationStopovers += Duration.between(previousSection.getArrivalTime(), section.getDepartureTime()).toMinutes();
                    }
                }
                previousSection = section;
            }
            return durationStopovers;
        } else {
            return 0L;
        }
    }
}
