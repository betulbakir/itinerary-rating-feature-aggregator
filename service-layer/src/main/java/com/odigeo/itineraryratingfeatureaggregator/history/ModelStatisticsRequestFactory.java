package com.odigeo.itineraryratingfeatureaggregator.history;

import com.google.inject.Singleton;
import com.odigeo.itineraryratingfeatureaggregator.StatisticElement;
import com.odigeo.itineraryratingfeatureaggregator.bean.ModelEvaluationRequested;
import com.odigeo.itineraryratingfeatureaggregator.bean.SearchResultsStatistics;

import java.util.HashMap;
import java.util.Map;

import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_CITY_AIRPORT_DURATION_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_CITY_AIRPORT_DURATION_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_CITY_AIRPORT_DURATION_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_DURATION_FLIGHT_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_DURATION_FLIGHT_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_DURATION_FLIGHT_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_DURATION_STOP_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_DURATION_STOP_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_DURATION_STOP_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_NUMBER_STOPS_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_NUMBER_STOPS_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_NUMBER_STOPS_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_PRICE;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_SEATS_AVAILABLE_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_SEATS_AVAILABLE_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_SEATS_AVAILABLE_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_STAY_TIME_SEC_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_CITY_AIRPORT_DURATION_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_CITY_AIRPORT_DURATION_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_CITY_AIRPORT_DURATION_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_DURATION_FLIGHT_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_DURATION_FLIGHT_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_DURATION_FLIGHT_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_DURATION_STOP_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_DURATION_STOP_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_DURATION_STOP_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_NUMBER_STOPS_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_NUMBER_STOPS_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_NUMBER_STOPS_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_PRICE;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_SEATS_AVAILABLE_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_SEATS_AVAILABLE_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_SEATS_AVAILABLE_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_STAY_TIME_SEC_TOTAL;

@Singleton
@SuppressWarnings("PMD.TooManyStaticImports")
public class ModelStatisticsRequestFactory {
    public Map<String, Number> getRequest(ModelEvaluationRequested modelEvaluation) {
        Map<String, Number> request = new HashMap<>();
        SearchResultsStatistics statistics = modelEvaluation.getSearchResultsStatistics();

        put(request, MAX_PRICE, statistics.getMaxPrice());
        put(request, MIN_PRICE, statistics.getMinPrice());

        put(request, MAX_DURATION_STOP_OUT, statistics.getMaxOutboundStopoverDuration());
        put(request, MIN_DURATION_STOP_OUT, statistics.getMinOutboundStopoverDuration());
        put(request, MAX_DURATION_STOP_IN, statistics.getMaxInboundStopoverDuration());
        put(request, MIN_DURATION_STOP_IN, statistics.getMinInboundStopoverDuration());
        put(request, MAX_DURATION_STOP_TOTAL, statistics.getMaxStopoverDuration());
        put(request, MIN_DURATION_STOP_TOTAL, statistics.getMinStopoverDuration());

        put(request, MAX_NUMBER_STOPS_OUT, statistics.getMaxOutboundNumberStopovers());
        put(request, MIN_NUMBER_STOPS_OUT, statistics.getMinOutboundNumberStopovers());
        put(request, MAX_NUMBER_STOPS_IN, statistics.getMaxInboundNumberStopovers());
        put(request, MIN_NUMBER_STOPS_IN, statistics.getMinInboundNumberStopovers());
        put(request, MAX_NUMBER_STOPS_TOTAL, statistics.getMaxNumberStopovers());
        put(request, MIN_NUMBER_STOPS_TOTAL, statistics.getMinNumberStopovers());

        put(request, MAX_DURATION_FLIGHT_OUT, statistics.getMaxOutboundFlightDuration());
        put(request, MIN_DURATION_FLIGHT_OUT, statistics.getMinOutboundFlightDuration());
        put(request, MAX_DURATION_FLIGHT_IN, statistics.getMaxInboundFlightDuration());
        put(request, MIN_DURATION_FLIGHT_IN, statistics.getMinInboundFlightDuration());
        put(request, MAX_DURATION_FLIGHT_TOTAL, statistics.getMaxFlightDuration());
        put(request, MIN_DURATION_FLIGHT_TOTAL, statistics.getMinFlightDuration());

        put(request, MAX_SEATS_AVAILABLE_OUT, statistics.getMaxOutboundNumberAvailableSeats());
        put(request, MIN_SEATS_AVAILABLE_OUT, statistics.getMinOutboundNumberAvailableSeats());
        put(request, MAX_SEATS_AVAILABLE_IN, statistics.getMaxInboundNumberAvailableSeats());
        put(request, MIN_SEATS_AVAILABLE_IN, statistics.getMinInboundNumberAvailableSeats());
        put(request, MAX_SEATS_AVAILABLE_TOTAL, statistics.getMaxNumberAvailableSeats());
        put(request, MIN_SEATS_AVAILABLE_TOTAL, statistics.getMinNumberAvailableSeats());

        put(request, MAX_CITY_AIRPORT_DURATION_OUT, statistics.getMaxOutboundCityToAirportTripDuration());
        put(request, MIN_CITY_AIRPORT_DURATION_OUT, statistics.getMinOutboundCityToAirportTripDuration());
        put(request, MAX_CITY_AIRPORT_DURATION_IN, statistics.getMaxInboundCityToAirportTripDuration());
        put(request, MIN_CITY_AIRPORT_DURATION_IN, statistics.getMinInboundCityToAirportTripDuration());
        put(request, MAX_CITY_AIRPORT_DURATION_TOTAL, statistics.getMaxCityToAirportTripDuration());
        put(request, MIN_CITY_AIRPORT_DURATION_TOTAL, statistics.getMinCityToAirportTripDuration());

        put(request, MAX_STAY_TIME_SEC_TOTAL, statistics.getMaxStayTimeSeconds());
        put(request, MIN_STAY_TIME_SEC_TOTAL, statistics.getMinStayTimeSeconds());

        return request;
    }

    private void put(Map<String, Number> statistics, StatisticElement key, Number value) {
        statistics.put(key.name(), value);
    }

}
