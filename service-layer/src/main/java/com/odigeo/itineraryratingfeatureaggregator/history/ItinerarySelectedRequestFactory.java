package com.odigeo.itineraryratingfeatureaggregator.history;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.itineraryratingfeatureaggregator.bean.BaggageConditions;
import com.odigeo.itineraryratingfeatureaggregator.bean.Itinerary;
import com.odigeo.itineraryratingfeatureaggregator.bean.ItineraryShoppingItem;
import com.odigeo.itineraryratingfeatureaggregator.bean.Segment;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.ItinerarySelectedRequest;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.SegmentRequest;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class ItinerarySelectedRequestFactory {

    private final SegmentSelectedRequestFactory segmentRequestFactory;

    @Inject
    public ItinerarySelectedRequestFactory(SegmentSelectedRequestFactory segmentRequestFactory) {
        this.segmentRequestFactory = segmentRequestFactory;
    }

    public ItinerarySelectedRequest getRequest(ItineraryShoppingItem itemAdded) {
        Itinerary itinerary = itemAdded.getItinerary();
        return new ItinerarySelectedRequest.Builder()
                .withApparentPriceInEur(itinerary.getApparentPriceEur())
                .withFreeCancellation(itinerary.isHasFreeCancellation())
                .withSegmentsRequest(getSegmentsRequest(itinerary.getSegments(), itinerary.getBaggageConditionsAdultTraveler()))
                .build();
    }

    private List<SegmentRequest> getSegmentsRequest(List<Segment> segments, List<BaggageConditions> itineraryConditions) {
        List<SegmentRequest> segmentRequests = new ArrayList<>();
        for (Segment segment : segments) {
            segmentRequests.add(segmentRequestFactory.getRequest(segment, itineraryConditions));
        }
        return segmentRequests;
    }
}
