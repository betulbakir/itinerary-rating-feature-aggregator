package com.odigeo.itineraryratingfeatureaggregator.history;

import com.odigeo.itineraryratingfeatureaggregator.bean.BaggageConditions;

import java.util.List;

public class BaggageSelectedRequestFactory {

    private static final String CHECK_IN = "CHECK_IN";
    private static final String CABIN = "CABIN";

    public String getRequest(List<BaggageConditions> segmentConditions, List<BaggageConditions> itineraryConditions) {
        String includedType = getBaggageIncludedType(itineraryConditions);
        if (CABIN.equals(includedType)) {
            includedType = getBaggageIncludedType(segmentConditions);
        }
        return includedType;
    }

    private String getBaggageIncludedType(List<BaggageConditions> conditions) {
        long numberOfConditionsWithIncludedBaggage = conditions.stream()
                .map(BaggageConditions::getBaggageDescriptor)
                .filter(descriptor -> descriptor.getNumKilos() > 0 || descriptor.getNumPieces() > 0)
                .count();
        return numberOfConditionsWithIncludedBaggage > 0 ? CHECK_IN : CABIN;
    }

}
