package com.odigeo.itineraryratingfeatureaggregator.history;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.itineraryratingfeatureaggregator.bean.MessageAggregatedData;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsConstants;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsHandler;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.ItineraryRatingFeatureHistoryService;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.exception.ItineraryRatingFeatureHistoryApiException;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.exception.RequestValidationException;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.UserSelectionRequest;
import org.apache.log4j.Logger;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

@Singleton
@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class ItineraryRatingFeatureHistoryHandler {

    private static final String ADD_USER_SELECTION_ERROR = "Error adding user selection to itinerary-rating-feature-history module";
    private static final String VALIDATION_REQUEST_ERROR = "Error validating request";
    private static final String CONNECTION_ERROR = "ConnectException trying to connect to module";
    private static final String SOCKET_TIMEOUT_ERROR = "SocketTimeoutException waiting for module";
    private static final String OTHER_ERROR = "Other error from module";

    private static final Logger LOGGER = Logger.getLogger(ItineraryRatingFeatureHistoryHandler.class);

    private final ItineraryRatingFeatureHistoryService service;
    private final MetricsHandler metricsHandler;
    private final UserSelectionRequestFactory requestFactory;

    @Inject
    public ItineraryRatingFeatureHistoryHandler(ItineraryRatingFeatureHistoryService service, MetricsHandler metricsHandler, UserSelectionRequestFactory requestFactory) {
        this.service = service;
        this.metricsHandler = metricsHandler;
        this.requestFactory = requestFactory;
    }

    public void sendUserData(MessageAggregatedData aggregatedData) {
        metricsHandler.startTimer(MetricsConstants.FEATURE_HISTORY_TIME);
        UserSelectionRequest request = requestFactory.getRequest(aggregatedData);
        try {
            service.addUserSelection(request);
        } catch (ItineraryRatingFeatureHistoryApiException e) {
            metricsHandler.counterMetric(MetricsConstants.FEATURE_HISTORY_API_EXCEPTION.getName());
            logError(ADD_USER_SELECTION_ERROR, e);
        } catch (RequestValidationException e) {
            metricsHandler.counterMetric(MetricsConstants.FEATURE_HISTORY_REQUEST_VALIDATION_EXCEPTION.getName());
            logErrorWithRequest(VALIDATION_REQUEST_ERROR, e, request);
        } catch (ConnectException e) {
            metricsHandler.counterMetric(MetricsConstants.FEATURE_HISTORY_CONNECT_EXCEPTION.getName());
            logError(CONNECTION_ERROR, e);
        } catch (SocketTimeoutException e) {
            metricsHandler.counterMetric(MetricsConstants.FEATURE_HISTORY_SOCKET_TIMEOUT_EXCEPTION.getName());
            logError(SOCKET_TIMEOUT_ERROR, e);
        } catch (Exception e) {
            metricsHandler.counterMetric(MetricsConstants.FEATURE_HISTORY_EXCEPTION.getName());
            logError(OTHER_ERROR, e);
        } finally {
            metricsHandler.stopTimer(MetricsConstants.FEATURE_HISTORY_TIME);
        }
    }

    private void logError(String message, Throwable e) {
        LOGGER.error(message, e);
    }

    private void logErrorWithRequest(String message, Throwable e, UserSelectionRequest request) {
        logError(new StringBuilder(message).append("Request: ").append(request).toString(), e);
    }
}
