package com.odigeo.itineraryratingfeatureaggregator.messaging.consumer;

import com.google.inject.Inject;
import com.odigeo.commons.messaging.MessageProcessor;
import com.odigeo.commons.messaging.domain.events.DomainEvent;
import com.odigeo.itineraryratingfeatureaggregator.bean.ItineraryShoppingItem;
import com.odigeo.itineraryratingfeatureaggregator.bean.MessageAggregatedData;
import com.odigeo.itineraryratingfeatureaggregator.cache.CacheKey;
import com.odigeo.itineraryratingfeatureaggregator.cache.CacheKeyType;
import com.odigeo.itineraryratingfeatureaggregator.cache.ModelEvaluationCache;
import com.odigeo.itineraryratingfeatureaggregator.cache.VisitCache;
import com.odigeo.itineraryratingfeatureaggregator.history.ItineraryRatingFeatureHistoryHandler;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsHandler;
import com.odigeo.retail.edreamssite.shoppingcart.domainevents.ShoppingItemAdded;
import com.odigeo.retail.meitineraryrating.request.domainevent.ModelEvaluationRequested;
import ma.glasnost.orika.MapperFacade;
import org.apache.avro.specific.SpecificRecord;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.time.Instant;

import static com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsConstants.MESSAGE_CONSUMED_ITEM_ADDED;
import static com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsConstants.MESSAGE_PROCESSED_ITEM_ADDED;
import static com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsConstants.MODEL_EVALUATION_NOT_FOUND;
import static com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsConstants.USER_COOKIE_NOT_FOUND;

public class ShoppingItemAddedProcessor implements MessageProcessor<DomainEvent<SpecificRecord>> {

    private static final Logger LOGGER = Logger.getLogger(ShoppingItemAddedProcessor.class);

    private final ModelEvaluationCache modelEvaluationCache;
    private final VisitCache visitCache;

    private final MetricsHandler metricsHandler;
    private final ItineraryRatingFeatureHistoryHandler handler;

    private final MapperFacade mapperFacade;

    @Inject
    public ShoppingItemAddedProcessor(ModelEvaluationCache modelEvaluationCache, VisitCache visitCache, MetricsHandler metricsHandler,
                                      ItineraryRatingFeatureHistoryHandler handler, MapperFacade mapperFacade) {
        this.modelEvaluationCache = modelEvaluationCache;
        this.visitCache = visitCache;
        this.metricsHandler = metricsHandler;
        this.handler = handler;
        this.mapperFacade = mapperFacade;
    }

    @Override
    public void onMessage(DomainEvent<SpecificRecord> domainEvent) {
        metricsHandler.sendCounterMetric(MESSAGE_CONSUMED_ITEM_ADDED);
        ShoppingItemAdded itemAdded = (ShoppingItemAdded) domainEvent.getPayload();
        String uniqueUserCookieId = getUniqueUserCookieId(itemAdded);

        if (hasBeenFetched(uniqueUserCookieId)) {
            processMessage(itemAdded, uniqueUserCookieId, domainEvent.getMetadata().getOccurrence());
        } else {
            metricsHandler.sendCounterMetric(USER_COOKIE_NOT_FOUND);
        }
    }

    private void processMessage(ShoppingItemAdded itemAdded, String uniqueUserCookieId, Instant eventOccurrence) {
        ModelEvaluationRequested evaluationRequested = getEvaluationRequested(itemAdded);
        if (hasBeenFetched(evaluationRequested)) {
            sendMessage(getAggregatedData(itemAdded, uniqueUserCookieId, eventOccurrence, evaluationRequested));
        } else {
            metricsHandler.sendCounterMetric(MODEL_EVALUATION_NOT_FOUND);
        }
    }

    private void sendMessage(MessageAggregatedData aggregatedData) {
        LOGGER.info(aggregatedData);
        handler.sendUserData(aggregatedData);
        metricsHandler.sendCounterMetric(MESSAGE_PROCESSED_ITEM_ADDED);
    }

    private boolean hasBeenFetched(Serializable valueFetched) {
        return valueFetched != null;
    }

    private ModelEvaluationRequested getEvaluationRequested(ShoppingItemAdded itemAdded) {
        return (ModelEvaluationRequested) modelEvaluationCache.fetchValue(new CacheKey(CacheKeyType.SEARCH_ID, itemAdded.getItineraryShoppingItem().getSearchId()));
    }

    private String getUniqueUserCookieId(ShoppingItemAdded itemAdded) {
        return (String) visitCache.fetchValue(new CacheKey(CacheKeyType.VISIT_ID, itemAdded.getItineraryShoppingItem().getVisitId()));
    }

    private MessageAggregatedData getAggregatedData(ShoppingItemAdded itemAdded, String uniqueUserCookieId, Instant eventOccurrence, ModelEvaluationRequested evaluationRequested) {
        return new MessageAggregatedData(getItineraryShoppingItemBean(itemAdded), getModelEvaluationRequestedBean(evaluationRequested), uniqueUserCookieId, eventOccurrence);
    }

    private ItineraryShoppingItem getItineraryShoppingItemBean(ShoppingItemAdded itemAdded) {
        return mapperFacade.map(itemAdded.getItineraryShoppingItem(), ItineraryShoppingItem.class);
    }
    private com.odigeo.itineraryratingfeatureaggregator.bean.ModelEvaluationRequested getModelEvaluationRequestedBean(ModelEvaluationRequested evaluationRequested) {
        return mapperFacade.map(evaluationRequested, com.odigeo.itineraryratingfeatureaggregator.bean.ModelEvaluationRequested.class);
    }

}
