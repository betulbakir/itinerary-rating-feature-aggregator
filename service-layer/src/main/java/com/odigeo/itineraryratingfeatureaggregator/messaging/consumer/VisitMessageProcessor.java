package com.odigeo.itineraryratingfeatureaggregator.messaging.consumer;

import com.google.inject.Inject;
import com.odigeo.commons.messaging.MessageProcessor;
import com.odigeo.itineraryratingfeatureaggregator.cache.CacheKey;
import com.odigeo.itineraryratingfeatureaggregator.cache.CacheKeyType;
import com.odigeo.itineraryratingfeatureaggregator.cache.VisitCache;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsConstants;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsHandler;
import com.odigeo.visit.message.v1.VisitMessage;
import org.apache.log4j.Logger;

public class VisitMessageProcessor implements MessageProcessor<VisitMessage> {

    private static final Logger LOGGER = Logger.getLogger(VisitMessageProcessor.class);

    private final VisitCache cache;
    private final InterfaceWhiteListConfiguration interfaceConfiguration;
    private final UserCookieValidator userCookieValidator;
    private final MetricsHandler metricsHandler;

    @Inject
    public VisitMessageProcessor(VisitCache cache, InterfaceWhiteListConfiguration interfaceConfiguration, MetricsHandler metricsHandler, UserCookieValidator userCookieValidator) {
        this.cache = cache;
        this.interfaceConfiguration = interfaceConfiguration;
        this.metricsHandler = metricsHandler;
        this.userCookieValidator = userCookieValidator;
    }

    @Override
    public void onMessage(VisitMessage message) {
        metricsHandler.counterMetric(MetricsConstants.MESSAGE_CONSUMED_VISIT.getName());
        String uniqueUserCookieId = message.getUniqueUserCookieId();
        if (interfaceConfiguration.getWhiteList().contains(message.getInterfaceId()) && userCookieValidator.isValidUserCookieId(uniqueUserCookieId)) {
            CacheKey key = new CacheKey(CacheKeyType.VISIT_ID, message.getVisitId());
            cache.addEntry(key, uniqueUserCookieId);
            LOGGER.info("Message inserted with visitId = " + message.getVisitId() + " and UniqueUserCookieId: " + uniqueUserCookieId);
            metricsHandler.counterMetric(MetricsConstants.MESSAGE_PROCESSED_VISIT.getName());
        }
    }

}
