package com.odigeo.itineraryratingfeatureaggregator.messaging.consumer;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;
import org.apache.log4j.Logger;

import java.util.regex.Pattern;

@Singleton
@ConfiguredInPropertiesFile
public class UserCookieValidator {

    private static final Logger LOGGER = Logger.getLogger(UserCookieValidator.class);

    private Pattern validUserCookiePattern;

    public void setValidUserCookieRegex(String validUserCookieRegex) {
        validUserCookiePattern = Pattern.compile(validUserCookieRegex);
    }

    public boolean isValidUserCookieId(String userCookieId) {
        boolean validCookie = userCookieId != null && validUserCookiePattern != null && validUserCookiePattern.matcher(userCookieId).matches();
        if (!validCookie) {
            LOGGER.warn("Invalid userCookieId: " + userCookieId);
        }
        return validCookie;
    }
}
