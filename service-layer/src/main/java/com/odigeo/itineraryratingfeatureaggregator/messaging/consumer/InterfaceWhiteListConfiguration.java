package com.odigeo.itineraryratingfeatureaggregator.messaging.consumer;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Singleton
@ConfiguredInPropertiesFile
public class InterfaceWhiteListConfiguration {

    private final Set<Integer> whiteListInterfaceIds = new HashSet<>();

    public void setWhiteListedInterfaceIds(Integer... whiteListedInterfaceIds) {
        Collections.addAll(this.whiteListInterfaceIds, whiteListedInterfaceIds);
    }

    public Set<Integer> getWhiteList() {
        return whiteListInterfaceIds;
    }
}
