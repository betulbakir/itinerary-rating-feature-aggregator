package com.odigeo.itineraryratingfeatureaggregator.messaging.consumer;

import com.google.inject.Inject;
import com.odigeo.commons.messaging.MessageProcessor;
import com.odigeo.commons.messaging.domain.events.DomainEvent;
import com.odigeo.itineraryratingfeatureaggregator.cache.CacheKey;
import com.odigeo.itineraryratingfeatureaggregator.cache.CacheKeyType;
import com.odigeo.itineraryratingfeatureaggregator.cache.ModelEvaluationCache;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsConstants;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsHandler;
import com.odigeo.retail.meitineraryrating.request.domainevent.ModelEvaluationRequested;
import org.apache.avro.specific.SpecificRecord;
import org.apache.log4j.Logger;

public class ModelEvaluationRequestedProcessor implements MessageProcessor<DomainEvent<SpecificRecord>> {

    private static final Logger LOGGER = Logger.getLogger(ModelEvaluationRequestedProcessor.class);

    private final ModelEvaluationCache cache;
    private final MetricsHandler metricsHandler;

    @Inject
    public ModelEvaluationRequestedProcessor(ModelEvaluationCache cache, MetricsHandler metricsHandler) {
        this.cache = cache;
        this.metricsHandler = metricsHandler;
    }

    @Override
    public void onMessage(DomainEvent<SpecificRecord> domainEvent) {
        ModelEvaluationRequested modelEvaluation = (ModelEvaluationRequested) domainEvent.getPayload();
        CacheKey key = new CacheKey(CacheKeyType.SEARCH_ID, modelEvaluation.getSearchId());
        cache.addEntry(key, modelEvaluation);
        LOGGER.info("Message inserted with searchId = " + modelEvaluation.getSearchId() + " and ModelEvaluationRequested: " + modelEvaluation);
        metricsHandler.counterMetric(MetricsConstants.MESSAGE_PROCESSED_ME_ITINERARY_RATING.getName());
    }

}
