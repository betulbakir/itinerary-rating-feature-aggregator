package com.odigeo.itineraryratingfeatureaggregator.metrics;

import com.odigeo.commons.monitoring.metrics.Metric;
import com.odigeo.commons.monitoring.metrics.MetricsPolicy;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class Constant {

    private final String name;
    private final String tags;
    private final MetricsPolicy.Precision precision;
    private final MetricsPolicy.Retention retention;

    public Constant(String name, String tags, MetricsPolicy.Precision precision, MetricsPolicy.Retention retention) {
        this.name = name;
        this.tags = tags;
        this.precision = precision;
        this.retention = retention;
    }

    public String getName() {
        return this.name;
    }

    public static class Builder {
        private final String name;
        private String tags;
        private final MetricsPolicy.Precision precision;
        private final MetricsPolicy.Retention retention;

        public Builder(String name) {
            this.name = name;
            this.tags = StringUtils.EMPTY;
            this.precision = MetricsPolicy.Precision.MINUTELY;
            this.retention = MetricsPolicy.Retention.WEEK;
        }

        public Builder tag(String key, String value) {
            this.tags += key + Metric.SEPARATOR + value + Metric.SEPARATOR;
            return this;
        }

        public Constant build() {
            return new Constant(name, StringUtils.chop(tags), precision, retention);
        }
    }

    public Metric getMetric(Map<String, String> tagValues) {
        String metricTags = MetricsConstants.getMetricName(tags, tagValues);

        Metric.Builder builder = new Metric.Builder(name).policy(precision, retention);
        String[] metricTagsSplitted = StringUtils.split(metricTags, Metric.SEPARATOR);

        for (int i = 0; i < metricTagsSplitted.length - 1; i += 2) {
            builder.tag(metricTagsSplitted[i], metricTagsSplitted[i + 1]);
        }

        return builder.build();
    }

}
