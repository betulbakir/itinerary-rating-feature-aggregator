package com.odigeo.itineraryratingfeatureaggregator.metrics;

import java.util.Map;

public abstract class MetricsConstants {

    public static final String REGISTRY_NAME = "itinerary-rating-feature-aggregator";

    private static final String LABEL_TOPIC_NAME = "topicName";
    private static final String LABEL_EXCEPTION = "exception";

    public static final Constant MESSAGE_CONSUMED_VISIT = composeKafkaMetric("message_consumed_visit", "VISIT_INFORMATION_v1");
    public static final Constant MESSAGE_PROCESSED_VISIT = composeKafkaMetric("message_processed_visit", "VISIT_INFORMATION_v1");

    public static final Constant MESSAGE_CONSUMED_ITEM_ADDED = composeKafkaMetric("message_consumed_item_added", "SHOPPING_CART_SHOPPING_ITEM");
    public static final Constant MESSAGE_PROCESSED_ITEM_ADDED = composeKafkaMetric("message_processed_item_added", "SHOPPING_CART_SHOPPING_ITEM");

    public static final Constant MESSAGE_PROCESSED_ME_ITINERARY_RATING = composeKafkaMetric("message_processed_me_itinerary_rating", "ITINERARY_RATING_REQUEST");

    public static final Constant MODEL_EVALUATION_NOT_FOUND = composeMetric("model_evaluation_not_found");
    public static final Constant USER_COOKIE_NOT_FOUND = composeMetric("user_cookie_not_found");

    public static final Constant FEATURE_HISTORY_API_EXCEPTION = composeExceptionMetric("itinerary_rating_feature_history_api_exception", "ItineraryRatingFeatureHistoryApiException");
    public static final Constant FEATURE_HISTORY_REQUEST_VALIDATION_EXCEPTION = composeExceptionMetric("itinerary_rating_feature_history_request_validation_exception", "RequestValidationException");
    public static final Constant FEATURE_HISTORY_CONNECT_EXCEPTION = composeExceptionMetric("itinerary_rating_feature_history_connect_exception", "ConnectException");
    public static final Constant FEATURE_HISTORY_SOCKET_TIMEOUT_EXCEPTION = composeExceptionMetric("itinerary_rating_feature_history_socket_timeout_exception", "SocketTimeoutException");
    public static final Constant FEATURE_HISTORY_EXCEPTION = composeExceptionMetric("itinerary_rating_feature_history_exception", "Exception");
    public static final Constant FEATURE_HISTORY_TIME = composeMetric("itinerary_rating_feature_history_time");

    public static final Constant GEO_HANDLER_LOCATION_TIME = composeMetric("geo_handler_location_time");
    public static final Constant GEO_HANDLER_GEO_SERVICE_EXCEPTION = composeExceptionMetric("geo_handler_geo_service_exception", "GeoServiceException");
    public static final Constant GEO_HANDLER_GEO_NODE_NOT_FOUND_EXCEPTION = composeExceptionMetric("geo_handler_geo_node_not_found_exception", "GeoNodeNotFoundException");
    public static final Constant GEO_HANDLER_INVALID_PARAMETERS_EXCEPTION = composeExceptionMetric("geo_handler_invalid_parameters_exception", "InvalidParametersException");

    public static String getMetricName(String metricConstant, Map<String, String> tagValues) {
        String metric = metricConstant;
        if (tagValues != null) {
            for (Map.Entry<String, String> entry : tagValues.entrySet()) {
                metric = metric.replace(entry.getKey(), entry.getValue());
            }
        }
        return metric;
    }

    private static Constant composeKafkaMetric(String metricName, String topicName) {
        return new Constant.Builder(metricName)
                .tag(LABEL_TOPIC_NAME, topicName)
                .build();
    }

    private static Constant composeExceptionMetric(String metricName, String exception) {
        return new Constant.Builder(metricName)
                .tag(LABEL_EXCEPTION, exception)
                .build();
    }

    private static Constant composeMetric(String metricName) {
        return new Constant.Builder(metricName)
                .build();
    }

}
