package com.odigeo.itineraryratingfeatureaggregator.metrics;

import com.google.inject.Singleton;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Singleton
@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class MetricsHandler {

    private static final Logger logger = LoggerFactory.getLogger(MetricsHandler.class.getName());

    private static final Map<String, String> EMPTY_TAG_MAP = Collections.emptyMap();

    private static final Map<String, Constant> METRIC_MAP = new HashMap<>();
    private static final String METRIC_WITH_AN_INVALID_VALUE_METRIC = "Metric with an invalid value. Metric: ";

    static {
        Field[] fields = MetricsConstants.class.getFields();
        for (Field field : fields) {
            if (field.getType() == Constant.class) {
                try {
                    METRIC_MAP.put(((Constant) field.get(null)).getName(), (Constant) field.get(null));
                } catch (IllegalAccessException e) {
                    logger.warn("The constant has an invalid access modifier: " + field.getName());
                }
            }
        }
    }

    public void counterMetric(String key, Map<String, String> tagValues) {
        Constant constant = METRIC_MAP.get(key);
        try {
            if (Objects.nonNull(constant)) {
                MetricsUtils.incrementCounter(constant.getMetric(tagValues), MetricsConstants.REGISTRY_NAME);
            } else {
                logger.warn(METRIC_WITH_AN_INVALID_VALUE_METRIC + key);
            }
        } catch (RuntimeException e) {
            logger.warn(METRIC_WITH_AN_INVALID_VALUE_METRIC + key);
        }
    }

    public void startTimer(String key, Map<String, String> tagValues) {
        Constant constant = METRIC_MAP.get(key);
        try {
            if (Objects.nonNull(constant)) {
                MetricsUtils.startTimer(constant.getMetric(tagValues),  MetricsConstants.REGISTRY_NAME);
            } else {
                logger.warn(METRIC_WITH_AN_INVALID_VALUE_METRIC + key);
            }
        } catch (RuntimeException e) {
            logger.warn(METRIC_WITH_AN_INVALID_VALUE_METRIC + key);
        }
    }

    public void stopTimer(String key, Map<String, String> tagValues) {
        Constant constant = METRIC_MAP.get(key);
        try {
            if (Objects.nonNull(constant)) {
                MetricsUtils.stopTimer(constant.getMetric(tagValues),  MetricsConstants.REGISTRY_NAME);
            } else {
                logger.warn(METRIC_WITH_AN_INVALID_VALUE_METRIC + key);
            }
        } catch (RuntimeException e) {
            logger.warn(METRIC_WITH_AN_INVALID_VALUE_METRIC + key);
        }
    }

    public void counterMetric(String key) {
        counterMetric(key, EMPTY_TAG_MAP);
    }

    public void sendCounterMetric(Constant constant) {
        counterMetric(constant.getName());
    }

    public void startTimer(String key) {
        startTimer(key, EMPTY_TAG_MAP);
    }

    public void startTimer(Constant constant) {
        startTimer(constant.getName());
    }

    public void stopTimer(String key) {
        stopTimer(key, EMPTY_TAG_MAP);
    }

    public void stopTimer(Constant constant) {
        stopTimer(constant.getName());
    }
}

