package com.odigeo.itineraryratingfeatureaggregator.geoservice;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.geoapi.v4.GeoNodeNotFoundException;
import com.odigeo.geoapi.v4.GeoServiceException;
import com.odigeo.geoapi.v4.InvalidParametersException;
import com.odigeo.geoapi.v4.ItineraryLocationService;
import com.odigeo.geoapi.v4.responses.ItineraryLocation;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsConstants;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsHandler;
import org.apache.log4j.Logger;

import java.util.Optional;

@Singleton
public class GeoServiceHandler {

    private static final Logger LOGGER = Logger.getLogger(GeoServiceHandler.class);

    private static final String ERROR_GEOSERVICE_EXCEPTION = "GeoService has launched an exception";

    private final ItineraryLocationService service;
    private final MetricsHandler metricsHandler;

    @Inject
    public GeoServiceHandler(ItineraryLocationService service, MetricsHandler metricsHandler) {
        this.service = service;
        this.metricsHandler = metricsHandler;
    }

    public Optional<ItineraryLocation> getLocation(String iata) {
        metricsHandler.startTimer(MetricsConstants.GEO_HANDLER_LOCATION_TIME);
        try {
            return Optional.ofNullable(service.getItineraryLocationByIataCode(iata));
        } catch (GeoServiceException e) {
            metricsHandler.sendCounterMetric(MetricsConstants.GEO_HANDLER_GEO_SERVICE_EXCEPTION);
            LOGGER.error(ERROR_GEOSERVICE_EXCEPTION, e);
        } catch (GeoNodeNotFoundException e) {
            metricsHandler.sendCounterMetric(MetricsConstants.GEO_HANDLER_GEO_NODE_NOT_FOUND_EXCEPTION);
            LOGGER.error(ERROR_GEOSERVICE_EXCEPTION, e);
        } catch (InvalidParametersException e) {
            metricsHandler.sendCounterMetric(MetricsConstants.GEO_HANDLER_INVALID_PARAMETERS_EXCEPTION);
            LOGGER.error(ERROR_GEOSERVICE_EXCEPTION, e);
        } finally {
            metricsHandler.stopTimer(MetricsConstants.GEO_HANDLER_LOCATION_TIME);
        }
        return Optional.empty();
    }

}
