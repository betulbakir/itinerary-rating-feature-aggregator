package com.odigeo.itineraryratingfeatureaggregator.geoservice;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.geoapi.v4.GeoNodeNotFoundException;
import com.odigeo.geoapi.v4.GeoServiceException;
import com.odigeo.geoapi.v4.InvalidParametersException;
import com.odigeo.geoapi.v4.ItineraryLocationService;
import com.odigeo.geoapi.v4.responses.Airport;
import com.odigeo.geoapi.v4.responses.ItineraryLocation;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsConstants;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsHandler;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Optional;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GeoServiceHandlerTest {

    private static final String IATA_CODE = "IATA_CODE";
    private static final String WRONG_IATA_CODE = "WRONG_IATA_CODE";
    private static final String MESSAGE = "MESSAGE";

    @Mock
    private MetricsHandler metricsHandler;
    @Mock
    private ItineraryLocationService service;

    private GeoServiceHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            public void configure() {
                bind(MetricsHandler.class).toInstance(metricsHandler);
                bind(ItineraryLocationService.class).toInstance(service);
            }
        });
        handler = ConfigurationEngine.getInstance(GeoServiceHandler.class);

    }

    @Test
    public void testSendUserData() throws Exception {
        Airport airport = mock(Airport.class);
        when(service.getItineraryLocationByIataCode(IATA_CODE)).thenReturn(airport);

        Optional<ItineraryLocation> location = handler.getLocation(IATA_CODE);

        verify(metricsHandler).startTimer(MetricsConstants.GEO_HANDLER_LOCATION_TIME);
        verify(metricsHandler).stopTimer(MetricsConstants.GEO_HANDLER_LOCATION_TIME);
        assertEquals(location.get(), airport);
    }

    @Test
    public void testSendUserDataWhenWrongIataCode() throws Exception {
        when(service.getItineraryLocationByIataCode(WRONG_IATA_CODE)).thenReturn(null);

        Optional<ItineraryLocation> location = handler.getLocation(IATA_CODE);

        assertEquals(location, Optional.empty());
    }

    @Test
    public void testSendUserDataWhenGeoServiceException() throws Exception {
        doThrow(new GeoServiceException(MESSAGE)).when(service).getItineraryLocationByIataCode(IATA_CODE);

        Optional<ItineraryLocation> location = handler.getLocation(IATA_CODE);

        verify(metricsHandler).startTimer(MetricsConstants.GEO_HANDLER_LOCATION_TIME);
        verify(metricsHandler).stopTimer(MetricsConstants.GEO_HANDLER_LOCATION_TIME);
        verify(metricsHandler).sendCounterMetric(MetricsConstants.GEO_HANDLER_GEO_SERVICE_EXCEPTION);
    }

    @Test
    public void testSendUserDataWhenGeoNodeNotFoundException() throws Exception {
        doThrow(new GeoNodeNotFoundException(MESSAGE)).when(service).getItineraryLocationByIataCode(IATA_CODE);

        Optional<ItineraryLocation> location = handler.getLocation(IATA_CODE);

        verify(metricsHandler).startTimer(MetricsConstants.GEO_HANDLER_LOCATION_TIME);
        verify(metricsHandler).stopTimer(MetricsConstants.GEO_HANDLER_LOCATION_TIME);
        verify(metricsHandler).sendCounterMetric(MetricsConstants.GEO_HANDLER_GEO_NODE_NOT_FOUND_EXCEPTION);
    }

    @Test
    public void testSendUserDataWhenInvalidParametersException() throws Exception {
        doThrow(new InvalidParametersException(MESSAGE)).when(service).getItineraryLocationByIataCode(IATA_CODE);

        Optional<ItineraryLocation> location = handler.getLocation(IATA_CODE);

        verify(metricsHandler).startTimer(MetricsConstants.GEO_HANDLER_LOCATION_TIME);
        verify(metricsHandler).stopTimer(MetricsConstants.GEO_HANDLER_LOCATION_TIME);
        verify(metricsHandler).sendCounterMetric(MetricsConstants.GEO_HANDLER_INVALID_PARAMETERS_EXCEPTION);
    }

}
