package com.odigeo.itineraryratingfeatureaggregator.history;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryratingfeatureaggregator.bean.Segment;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.GeographicalInformationRequest;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.SegmentRequest;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

import static com.odigeo.itineraryratingfeatureaggregator.ZoneDateTimeHelper.UTC;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class SegmentSelectedRequestFactoryTest {

    private static final String CABIN = "CABIN";
    private static final Long STOPOVER_DURATION = 2L;
    private static final Long FLIGHT_DURATION = 20L;
    private static final Integer NUM_STOPOVERS = 3;
    private static final Integer NUM_SEATS = 4;
    private static final String MARKETING_CODE = "code";
    private static final Instant AN_INSTANT = Instant.ofEpochMilli(1583330525L);
    private static final Instant ANOTHER_INSTANT = Instant.ofEpochMilli(1583220025L);

    @Mock
    private BaggageSelectedRequestFactory baggageRequestFactory;
    @Mock
    private GeoInfoRequestFactory geoInfoRequestFactory;
    @Mock
    private StopoverDurationCalculator stopoverDurationCalculator;

    @Mock
    private GeographicalInformationRequest geoRequest;


    private SegmentSelectedRequestFactory factory;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            public void configure() {
                bind(BaggageSelectedRequestFactory.class).toInstance(baggageRequestFactory);
                bind(GeoInfoRequestFactory.class).toInstance(geoInfoRequestFactory);
                bind(StopoverDurationCalculator.class).toInstance(stopoverDurationCalculator);
            }
        });
        factory = ConfigurationEngine.getInstance(SegmentSelectedRequestFactory.class);
        when(baggageRequestFactory.getRequest(any(List.class), any(List.class))).thenReturn(CABIN);
        when(geoInfoRequestFactory.getRequest(any(Segment.class))).thenReturn(geoRequest);
        when(stopoverDurationCalculator.getStopoverDuration(any(Segment.class))).thenReturn(STOPOVER_DURATION);
    }

    @Test
    public void testGetRequest() {

        Segment segment = mock(Segment.class);
        when(segment.getDepartureTime()).thenReturn(ANOTHER_INSTANT);
        when(segment.getArrivalTime()).thenReturn(AN_INSTANT);
        when(segment.getNumStopovers()).thenReturn(NUM_STOPOVERS);
        when(segment.getMarketingCarrierCode()).thenReturn(MARKETING_CODE);
        when(segment.getSeatsAvailable()).thenReturn(NUM_SEATS);
        when(segment.getDuration()).thenReturn(FLIGHT_DURATION);

        SegmentRequest request = factory.getRequest(segment, Collections.emptyList());

        assertEquals(request.getDepartureDateUTC(), ANOTHER_INSTANT.atZone(UTC));
        assertEquals(request.getArrivalDateUTC(), AN_INSTANT.atZone(UTC));
        assertEquals(request.getNumStopovers(), NUM_STOPOVERS);
        assertEquals(request.getMarketingCarrierCode(), MARKETING_CODE);
        assertEquals(request.getSeatsAvailable(), NUM_SEATS);
        assertEquals(request.getBaggageIncludedType(), CABIN);
        assertEquals(request.getGeographicalInformationRequest(), geoRequest);
        assertEquals(request.getDurStopovers(), STOPOVER_DURATION);
        assertEquals(request.getFlightDuration(), FLIGHT_DURATION);
    }
}
