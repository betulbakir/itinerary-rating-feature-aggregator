package com.odigeo.itineraryratingfeatureaggregator.history;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.itineraryratingfeatureaggregator.StatisticElement;
import com.odigeo.itineraryratingfeatureaggregator.bean.ModelEvaluationRequested;
import com.odigeo.itineraryratingfeatureaggregator.bean.SearchResultsStatistics;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Map;

import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_CITY_AIRPORT_DURATION_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_CITY_AIRPORT_DURATION_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_CITY_AIRPORT_DURATION_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_DURATION_FLIGHT_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_DURATION_FLIGHT_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_DURATION_FLIGHT_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_DURATION_STOP_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_DURATION_STOP_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_DURATION_STOP_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_NUMBER_STOPS_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_NUMBER_STOPS_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_NUMBER_STOPS_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_PRICE;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_SEATS_AVAILABLE_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_SEATS_AVAILABLE_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_SEATS_AVAILABLE_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MAX_STAY_TIME_SEC_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_CITY_AIRPORT_DURATION_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_CITY_AIRPORT_DURATION_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_CITY_AIRPORT_DURATION_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_DURATION_FLIGHT_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_DURATION_FLIGHT_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_DURATION_FLIGHT_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_DURATION_STOP_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_DURATION_STOP_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_DURATION_STOP_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_NUMBER_STOPS_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_NUMBER_STOPS_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_NUMBER_STOPS_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_PRICE;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_SEATS_AVAILABLE_IN;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_SEATS_AVAILABLE_OUT;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_SEATS_AVAILABLE_TOTAL;
import static com.odigeo.itineraryratingfeatureaggregator.StatisticElement.MIN_STAY_TIME_SEC_TOTAL;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

@SuppressWarnings("PMD.TooManyStaticImports")
public class ModelStatisticsRequestFactoryTest {

    private static final BigDecimal MAX_BIG_DECIMAL = BigDecimal.TEN;
    private static final BigDecimal MIN_BIG_DECIMAL = BigDecimal.ONE;

    private static final Long MAX_DAYS_SEC = 100000L;
    private static final Long MIN_DAYS_SEC = 50000L;

    private static final Long MAX_CITY_AIRPORT_DURATION_OUT_VALUE = 1L;
    private static final Long MAX_CITY_AIRPORT_DURATION_IN_VALUE = 2L;
    private static final Long MAX_CITY_AIRPORT_DURATION_TOTAL_VALUE = 3L;
    private static final Long MIN_CITY_AIRPORT_DURATION_OUT_VALUE = 4L;
    private static final Long MIN_CITY_AIRPORT_DURATION_IN_VALUE = 5L;
    private static final Long MIN_CITY_AIRPORT_DURATION_TOTAL_VALUE = 6L;

    private static final Long MAX_DUR_OUT = 11L;
    private static final Long MAX_DUR_IN = 12L;
    private static final Long MAX_DUR_TOTAL = 13L;
    private static final Long MIN_DUR_OUT = 14L;
    private static final Long MIN_DUR_IN = 15L;
    private static final Long MIN_DUR_TOTAL = 16L;

    private static final Long MAX_LONG_STOP_NUM_OUT = 21L;
    private static final Long MAX_LONG_STOP_NUM_IN = 22L;
    private static final Long MAX_LONG_STOP_NUM_TOTAL = 23L;
    private static final Long MIN_LONG_STOP_NUM_OUT = 24L;
    private static final Long MIN_LONG_STOP_NUM_IN = 25L;
    private static final Long MIN_LONG_STOP_NUM_TOTAL = 26L;

    private static final Long MAX_LONG_STOP_DUR_OUT = 31L;
    private static final Long MAX_LONG_STOP_DUR_IN = 32L;
    private static final Long MAX_LONG_STOP_DUR_TOTAL = 33L;
    private static final Long MIN_LONG_STOP_DUR_OUT = 34L;
    private static final Long MIN_LONG_STOP_DUR_IN = 35L;
    private static final Long MIN_LONG_STOP_DUR_TOTAL = 35L;

    private static final Long MAX_LONG_SEATS_OUT = 41L;
    private static final Long MAX_LONG_SEATS_IN = 42L;
    private static final Long MAX_LONG_SEATS_TOTAL = 43L;
    private static final Long MIN_LONG_SEATS_OUT = 44L;
    private static final Long MIN_LONG_SEATS_IN = 45L;
    private static final Long MIN_LONG_SEATS_TOTAL = 46L;

    private ModelStatisticsRequestFactory factory;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init();
        factory = ConfigurationEngine.getInstance(ModelStatisticsRequestFactory.class);
    }

    @Test
    public void price() {
        Map<String, Number> request = factory.getRequest(getModelEvaluationRequested());

        assertEquals(request.get(getName(MAX_PRICE)), MAX_BIG_DECIMAL);
        assertEquals(request.get(getName(MIN_PRICE)), MIN_BIG_DECIMAL);
    }

    @Test
    public void stayTimeSecElement() {
        Map<String, Number> request = factory.getRequest(getModelEvaluationRequested());

        assertEquals(request.get(getName(MAX_STAY_TIME_SEC_TOTAL)), MAX_DAYS_SEC);
        assertEquals(request.get(getName(MIN_STAY_TIME_SEC_TOTAL)), MIN_DAYS_SEC);
    }

    @Test
    public void cityAirportDurations() {
        Map<String, Number> request = factory.getRequest(getModelEvaluationRequested());

        assertEquals(request.get(getName(MAX_CITY_AIRPORT_DURATION_OUT)), MAX_CITY_AIRPORT_DURATION_OUT_VALUE);
        assertEquals(request.get(getName(MIN_CITY_AIRPORT_DURATION_OUT)), MIN_CITY_AIRPORT_DURATION_OUT_VALUE);
        assertEquals(request.get(getName(MAX_CITY_AIRPORT_DURATION_IN)), MAX_CITY_AIRPORT_DURATION_IN_VALUE);
        assertEquals(request.get(getName(MIN_CITY_AIRPORT_DURATION_IN)), MIN_CITY_AIRPORT_DURATION_IN_VALUE);
        assertEquals(request.get(getName(MAX_CITY_AIRPORT_DURATION_TOTAL)), MAX_CITY_AIRPORT_DURATION_TOTAL_VALUE);
        assertEquals(request.get(getName(MIN_CITY_AIRPORT_DURATION_TOTAL)), MIN_CITY_AIRPORT_DURATION_TOTAL_VALUE);
    }

    @Test
    public void flightDurations() {
        Map<String, Number> request = factory.getRequest(getModelEvaluationRequested());

        assertEquals(request.get(getName(MAX_DURATION_FLIGHT_OUT)), MAX_DUR_OUT);
        assertEquals(request.get(getName(MIN_DURATION_FLIGHT_OUT)), MIN_DUR_OUT);
        assertEquals(request.get(getName(MAX_DURATION_FLIGHT_IN)), MAX_DUR_IN);
        assertEquals(request.get(getName(MIN_DURATION_FLIGHT_IN)), MIN_DUR_IN);
        assertEquals(request.get(getName(MAX_DURATION_FLIGHT_TOTAL)), MAX_DUR_TOTAL);
        assertEquals(request.get(getName(MIN_DURATION_FLIGHT_TOTAL)), MIN_DUR_TOTAL);
    }

    @Test
    public void stopoverNumbers() {
        Map<String, Number> request = factory.getRequest(getModelEvaluationRequested());

        assertEquals(request.get(getName(MAX_NUMBER_STOPS_OUT)), MAX_LONG_STOP_NUM_OUT);
        assertEquals(request.get(getName(MIN_NUMBER_STOPS_OUT)), MIN_LONG_STOP_NUM_OUT);
        assertEquals(request.get(getName(MAX_NUMBER_STOPS_IN)), MAX_LONG_STOP_NUM_IN);
        assertEquals(request.get(getName(MIN_NUMBER_STOPS_IN)), MIN_LONG_STOP_NUM_IN);
        assertEquals(request.get(getName(MAX_NUMBER_STOPS_TOTAL)), MAX_LONG_STOP_NUM_TOTAL);
        assertEquals(request.get(getName(MIN_NUMBER_STOPS_TOTAL)), MIN_LONG_STOP_NUM_TOTAL);
    }

    @Test
    public void stopoverDuration() {
        Map<String, Number> request = factory.getRequest(getModelEvaluationRequested());

        assertEquals(request.get(getName(MAX_DURATION_STOP_OUT)), MAX_LONG_STOP_DUR_OUT);
        assertEquals(request.get(getName(MIN_DURATION_STOP_OUT)), MIN_LONG_STOP_DUR_OUT);
        assertEquals(request.get(getName(MAX_DURATION_STOP_IN)), MAX_LONG_STOP_DUR_IN);
        assertEquals(request.get(getName(MIN_DURATION_STOP_IN)), MIN_LONG_STOP_DUR_IN);
        assertEquals(request.get(getName(MAX_DURATION_STOP_TOTAL)), MAX_LONG_STOP_DUR_TOTAL);
        assertEquals(request.get(getName(MIN_DURATION_STOP_TOTAL)), MIN_LONG_STOP_DUR_TOTAL);
    }

    @Test
    public void numberSeatsAvailable() {
        Map<String, Number> request = factory.getRequest(getModelEvaluationRequested());

        assertEquals(request.get(getName(MAX_SEATS_AVAILABLE_OUT)), MAX_LONG_SEATS_OUT);
        assertEquals(request.get(getName(MIN_SEATS_AVAILABLE_OUT)), MIN_LONG_SEATS_OUT);
        assertEquals(request.get(getName(MAX_SEATS_AVAILABLE_IN)), MAX_LONG_SEATS_IN);
        assertEquals(request.get(getName(MIN_SEATS_AVAILABLE_IN)), MIN_LONG_SEATS_IN);
        assertEquals(request.get(getName(MAX_SEATS_AVAILABLE_TOTAL)), MAX_LONG_SEATS_TOTAL);
        assertEquals(request.get(getName(MIN_SEATS_AVAILABLE_TOTAL)), MIN_LONG_SEATS_TOTAL);
    }

    private String getName(StatisticElement element) {
        return element.name();
    }

    private static ModelEvaluationRequested getModelEvaluationRequested() {
        ModelEvaluationRequested modelEvaluationRequested = mock(ModelEvaluationRequested.class);
        SearchResultsStatistics statistics = mock(SearchResultsStatistics.class);

        when(modelEvaluationRequested.getSearchResultsStatistics()).thenReturn(statistics);
        when(modelEvaluationRequested.getSearchResultsStatistics()).thenReturn(statistics);

        when(statistics.getMaxPrice()).thenReturn(MAX_BIG_DECIMAL);
        when(statistics.getMinPrice()).thenReturn(MIN_BIG_DECIMAL);

        when(statistics.getMaxOutboundStopoverDuration()).thenReturn(MAX_LONG_STOP_DUR_OUT);
        when(statistics.getMinOutboundStopoverDuration()).thenReturn(MIN_LONG_STOP_DUR_OUT);
        when(statistics.getMaxInboundStopoverDuration()).thenReturn(MAX_LONG_STOP_DUR_IN);
        when(statistics.getMinInboundStopoverDuration()).thenReturn(MIN_LONG_STOP_DUR_IN);
        when(statistics.getMaxStopoverDuration()).thenReturn(MAX_LONG_STOP_DUR_TOTAL);
        when(statistics.getMinStopoverDuration()).thenReturn(MIN_LONG_STOP_DUR_TOTAL);

        when(statistics.getMaxOutboundNumberStopovers()).thenReturn(MAX_LONG_STOP_NUM_OUT);
        when(statistics.getMinOutboundNumberStopovers()).thenReturn(MIN_LONG_STOP_NUM_OUT);
        when(statistics.getMaxInboundNumberStopovers()).thenReturn(MAX_LONG_STOP_NUM_IN);
        when(statistics.getMinInboundNumberStopovers()).thenReturn(MIN_LONG_STOP_NUM_IN);
        when(statistics.getMaxNumberStopovers()).thenReturn(MAX_LONG_STOP_NUM_TOTAL);
        when(statistics.getMinNumberStopovers()).thenReturn(MIN_LONG_STOP_NUM_TOTAL);

        when(statistics.getMaxOutboundFlightDuration()).thenReturn(MAX_DUR_OUT);
        when(statistics.getMinOutboundFlightDuration()).thenReturn(MIN_DUR_OUT);
        when(statistics.getMaxInboundFlightDuration()).thenReturn(MAX_DUR_IN);
        when(statistics.getMinInboundFlightDuration()).thenReturn(MIN_DUR_IN);
        when(statistics.getMaxFlightDuration()).thenReturn(MAX_DUR_TOTAL);
        when(statistics.getMinFlightDuration()).thenReturn(MIN_DUR_TOTAL);

        when(statistics.getMaxOutboundNumberAvailableSeats()).thenReturn(MAX_LONG_SEATS_OUT);
        when(statistics.getMinOutboundNumberAvailableSeats()).thenReturn(MIN_LONG_SEATS_OUT);
        when(statistics.getMaxInboundNumberAvailableSeats()).thenReturn(MAX_LONG_SEATS_IN);
        when(statistics.getMinInboundNumberAvailableSeats()).thenReturn(MIN_LONG_SEATS_IN);
        when(statistics.getMaxNumberAvailableSeats()).thenReturn(MAX_LONG_SEATS_TOTAL);
        when(statistics.getMinNumberAvailableSeats()).thenReturn(MIN_LONG_SEATS_TOTAL);

        when(statistics.getMaxOutboundCityToAirportTripDuration()).thenReturn(MAX_CITY_AIRPORT_DURATION_OUT_VALUE);
        when(statistics.getMinOutboundCityToAirportTripDuration()).thenReturn(MIN_CITY_AIRPORT_DURATION_OUT_VALUE);
        when(statistics.getMaxInboundCityToAirportTripDuration()).thenReturn(MAX_CITY_AIRPORT_DURATION_IN_VALUE);
        when(statistics.getMinInboundCityToAirportTripDuration()).thenReturn(MIN_CITY_AIRPORT_DURATION_IN_VALUE);
        when(statistics.getMaxCityToAirportTripDuration()).thenReturn(MAX_CITY_AIRPORT_DURATION_TOTAL_VALUE);
        when(statistics.getMinCityToAirportTripDuration()).thenReturn(MIN_CITY_AIRPORT_DURATION_TOTAL_VALUE);

        when(statistics.getMaxStayTimeSeconds()).thenReturn(MAX_DAYS_SEC);
        when(statistics.getMinStayTimeSeconds()).thenReturn(MIN_DAYS_SEC);

        return modelEvaluationRequested;
    }
}
