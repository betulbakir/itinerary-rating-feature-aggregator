package com.odigeo.itineraryratingfeatureaggregator.history;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryratingfeatureaggregator.bean.MessageAggregatedData;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsConstants;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsHandler;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.ItineraryRatingFeatureHistoryService;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.exception.ItineraryRatingFeatureHistoryApiException;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.exception.RequestValidationException;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.UserSelectionRequest;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ItineraryRatingFeatureHistoryHandlerTest {

    private static final String MESSAGE = "MESSAGE";

    @Mock
    private MetricsHandler metricsHandler;
    @Mock
    private ItineraryRatingFeatureHistoryService service;
    @Mock
    private UserSelectionRequestFactory requestFactory;
    @Mock
    private UserSelectionRequest request;

    private ItineraryRatingFeatureHistoryHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            public void configure() {
                bind(MetricsHandler.class).toInstance(metricsHandler);
                bind(ItineraryRatingFeatureHistoryService.class).toInstance(service);
                bind(UserSelectionRequestFactory.class).toInstance(requestFactory);
            }
        });
        handler = ConfigurationEngine.getInstance(ItineraryRatingFeatureHistoryHandler.class);
        when(requestFactory.getRequest(any(MessageAggregatedData.class))).thenReturn(request);
    }

    @Test
    public void testSendUserData() throws Exception {
        MessageAggregatedData data = mock(MessageAggregatedData.class);

        handler.sendUserData(data);

        verify(requestFactory).getRequest(data);
        verify(service).addUserSelection(request);
    }

    @Test
    public void testSendUserDataWhenItineraryRatingFeatureHistoryApiException() throws Exception {
        MessageAggregatedData data = mock(MessageAggregatedData.class);
        doThrow(new ItineraryRatingFeatureHistoryApiException(MESSAGE)).when(service).addUserSelection(request);

        handler.sendUserData(data);

        verify(metricsHandler).counterMetric(MetricsConstants.FEATURE_HISTORY_API_EXCEPTION.getName());
    }

    @Test
    public void testSendUserDataWhenRequestValidationException() throws Exception {
        MessageAggregatedData data = mock(MessageAggregatedData.class);
        doThrow(new RequestValidationException(MESSAGE)).when(service).addUserSelection(request);

        handler.sendUserData(data);

        verify(metricsHandler).counterMetric(MetricsConstants.FEATURE_HISTORY_REQUEST_VALIDATION_EXCEPTION.getName());
    }

    @Test
    public void testSendUserDataWhenConnectException() throws Exception {
        MessageAggregatedData data = mock(MessageAggregatedData.class);
        doThrow(new ConnectException()).when(service).addUserSelection(request);

        handler.sendUserData(data);

        verify(metricsHandler).counterMetric(MetricsConstants.FEATURE_HISTORY_CONNECT_EXCEPTION.getName());
    }

    @Test
    public void testSendUserDataWhenSocketTimeoutException() throws Exception {
        MessageAggregatedData data = mock(MessageAggregatedData.class);
        doThrow(new SocketTimeoutException()).when(service).addUserSelection(request);

        handler.sendUserData(data);

        verify(metricsHandler).counterMetric(MetricsConstants.FEATURE_HISTORY_SOCKET_TIMEOUT_EXCEPTION.getName());
    }

    @Test
    public void testSendUserDataWhenGenericException() throws Exception {
        MessageAggregatedData data = mock(MessageAggregatedData.class);
        doThrow(new RuntimeException()).when(service).addUserSelection(request);

        handler.sendUserData(data);

        verify(metricsHandler).counterMetric(MetricsConstants.FEATURE_HISTORY_EXCEPTION.getName());
    }
}
