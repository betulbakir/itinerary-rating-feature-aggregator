package com.odigeo.itineraryratingfeatureaggregator.history;

import com.odigeo.itineraryratingfeatureaggregator.bean.Section;
import com.odigeo.itineraryratingfeatureaggregator.bean.Segment;
import org.testng.annotations.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class StopoverDurationCalculatorTest {

    private static final int DIRECT_FLIGHT_STOPOVERS = 0;
    private static final int FEW_FLIGHT_STOPOVERS = 1;

    private static final Instant DEPARTURE_TIME_1 = Instant.ofEpochMilli(1583330525L);
    private static final Instant ARRIVAL_TIME_1 = DEPARTURE_TIME_1.plus(1, ChronoUnit.HOURS);
    private static final Instant DEPARTURE_TIME_2 = ARRIVAL_TIME_1.plus(10, ChronoUnit.HOURS);
    private static final Instant ARRIVAL_TIME_2 = DEPARTURE_TIME_2.plus(2, ChronoUnit.HOURS);
    private static final Instant DEPARTURE_TIME_3 = ARRIVAL_TIME_2.plus(5, ChronoUnit.HOURS);
    private static final Instant ARRIVAL_TIME_3 = DEPARTURE_TIME_3.plus(2, ChronoUnit.HOURS);

    @Test
    public void durationWhenDirect() {
        Segment segment = mock(Segment.class);
        when(segment.getNumStopovers()).thenReturn(DIRECT_FLIGHT_STOPOVERS);

        long actualDuration = new StopoverDurationCalculator().getStopoverDuration(segment);

        assertEquals(actualDuration, 0L);
    }

    @Test
    public void durationWhenOneStopover() {
        Segment segment = mock(Segment.class);
        when(segment.getNumStopovers()).thenReturn(FEW_FLIGHT_STOPOVERS);

        List<Section> sections = new ArrayList<>();
        sections.add(getSection(DEPARTURE_TIME_1, ARRIVAL_TIME_1));
        sections.add(getSection(DEPARTURE_TIME_2, ARRIVAL_TIME_2));
        when(segment.getSections()).thenReturn(sections);

        long actualDuration = new StopoverDurationCalculator().getStopoverDuration(segment);

        assertEquals(actualDuration, 600L);
    }

    @Test
    public void durationWhenTwoStopover() {
        Segment segment = mock(Segment.class);
        when(segment.getNumStopovers()).thenReturn(FEW_FLIGHT_STOPOVERS);

        List<Section> sections = new ArrayList<>();
        sections.add(getSection(DEPARTURE_TIME_1, ARRIVAL_TIME_1));
        sections.add(getSection(DEPARTURE_TIME_2, ARRIVAL_TIME_2));
        sections.add(getSection(DEPARTURE_TIME_3, ARRIVAL_TIME_3));
        when(segment.getSections()).thenReturn(sections);

        long actualDurationMinutes = new StopoverDurationCalculator().getStopoverDuration(segment);

        assertEquals(actualDurationMinutes, 900L);
    }

    private Section getSection(Instant departureTime, Instant arrivalTime) {
        Section section = mock(Section.class);
        when(section.getDepartureTime()).thenReturn(departureTime);
        when(section.getArrivalTime()).thenReturn(arrivalTime);
        return section;
    }
}
