package com.odigeo.itineraryratingfeatureaggregator.history;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.geoapi.v4.responses.Airport;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.responses.Continent;
import com.odigeo.geoapi.v4.responses.Coordinates;
import com.odigeo.geoapi.v4.responses.Country;
import com.odigeo.geoapi.v4.responses.ItineraryLocation;
import com.odigeo.itineraryratingfeatureaggregator.bean.Segment;
import com.odigeo.itineraryratingfeatureaggregator.geoservice.GeoServiceHandler;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.GeographicalInformationRequest;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class GeoInfoRequestFactoryTest {

    private static final String DEPARTURE_AIRPORT = "GRO";
    private static final String DEPARTURE_CITY = "BCN";
    private static final String DEPARTURE_COUNTRY = "ESP";
    private static final int DEPARTURE_CONTINENT = 1;
    private static final BigDecimal DEPARTURE_LATITUDE = new BigDecimal("1.1");
    private static final BigDecimal DEPARTURE_LONGITUDE = new BigDecimal("2.2");

    private static final String ARRIVAL_AIRPORT = "STN";
    private static final String ARRIVAL_CITY = "LON";
    private static final String ARRIVAL_COUNTRY = "UK";
    private static final int ARRIVAL_CONTINENT = 2;
    private static final BigDecimal ARRIVAL_LATITUDE = new BigDecimal("3.3");;
    private static final BigDecimal ARRIVAL_LONGITUDE = new BigDecimal("4.4");

    @Mock
    private GeoServiceHandler serviceHandler;

    private GeoInfoRequestFactory factory;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            public void configure() {
                bind(GeoServiceHandler.class).toInstance(serviceHandler);
            }
        });
        factory = ConfigurationEngine.getInstance(GeoInfoRequestFactory.class);
    }

    @Test
    public void testGetRequest() {

        Airport departureAirportMock = getAirportMock(DEPARTURE_COUNTRY, DEPARTURE_CONTINENT, DEPARTURE_LATITUDE, DEPARTURE_LONGITUDE);
        Airport arrivalAirportMock = getAirportMock(ARRIVAL_COUNTRY, ARRIVAL_CONTINENT, ARRIVAL_LATITUDE, ARRIVAL_LONGITUDE);
        when(serviceHandler.getLocation(DEPARTURE_AIRPORT)).thenReturn(Optional.of(departureAirportMock));
        when(serviceHandler.getLocation(ARRIVAL_AIRPORT)).thenReturn(Optional.of(arrivalAirportMock));

        GeographicalInformationRequest request = factory.getRequest(getSegment());

        assertEquals(request.getDepartureAirport(), DEPARTURE_AIRPORT);
        assertEquals(request.getDepartureCity(), DEPARTURE_CITY);
        assertEquals(request.getDepartureCountry(), DEPARTURE_COUNTRY);
        assertEquals(request.getDepartureContinentId(), Integer.toString(DEPARTURE_CONTINENT));
        assertEquals(request.getDepartureCoordinates().getLatitude(), DEPARTURE_LATITUDE);
        assertEquals(request.getDepartureCoordinates().getLongitude(), DEPARTURE_LONGITUDE);
        assertEquals(request.getArrivalAirport(), ARRIVAL_AIRPORT);
        assertEquals(request.getArrivalCity(), ARRIVAL_CITY);
        assertEquals(request.getArrivalCountry(), ARRIVAL_COUNTRY);
        assertEquals(request.getArrivalContinentId(), Integer.toString(ARRIVAL_CONTINENT));
        assertEquals(request.getArrivalCoordinates().getLatitude(), ARRIVAL_LATITUDE);
        assertEquals(request.getArrivalCoordinates().getLongitude(), ARRIVAL_LONGITUDE);
    }

    @Test
    public void testGetRequestWhenEmptyAirport() {

        Optional<ItineraryLocation> emptyDepartureAirport = Optional.empty();
        Airport arrivalAirportMock = getAirportMock(ARRIVAL_COUNTRY, ARRIVAL_CONTINENT, ARRIVAL_LATITUDE, ARRIVAL_LONGITUDE);
        when(serviceHandler.getLocation(DEPARTURE_AIRPORT)).thenReturn(emptyDepartureAirport);
        when(serviceHandler.getLocation(ARRIVAL_AIRPORT)).thenReturn(Optional.of(arrivalAirportMock));

        GeographicalInformationRequest request = factory.getRequest(getSegment());

        assertEquals(request.getDepartureAirport(), DEPARTURE_AIRPORT);
        assertEquals(request.getDepartureCity(), DEPARTURE_CITY);
        assertNull(request.getDepartureCountry());
        assertNull(request.getDepartureContinentId());
        assertNull(request.getDepartureCoordinates());
        assertEquals(request.getArrivalAirport(), ARRIVAL_AIRPORT);
        assertEquals(request.getArrivalCity(), ARRIVAL_CITY);
        assertEquals(request.getArrivalCountry(), ARRIVAL_COUNTRY);
        assertEquals(request.getArrivalContinentId(), Integer.toString(ARRIVAL_CONTINENT));
        assertEquals(request.getArrivalCoordinates().getLatitude(), ARRIVAL_LATITUDE);
        assertEquals(request.getArrivalCoordinates().getLongitude(), ARRIVAL_LONGITUDE);
    }

    private Segment getSegment() {
        Segment segment = mock(Segment.class);
        when(segment.getDepartureAirportIataCode()).thenReturn(DEPARTURE_AIRPORT);
        when(segment.getDepartureCityIataCode()).thenReturn(DEPARTURE_CITY);
        when(segment.getArrivalAirportIataCode()).thenReturn(ARRIVAL_AIRPORT);
        when(segment.getArrivalCityIataCode()).thenReturn(ARRIVAL_CITY);
        return segment;
    }

    private Airport getAirportMock(String countryCode, int continentId, BigDecimal latitude, BigDecimal longitude) {
        Airport airport = mock(Airport.class);
        City city = getCity(getCountry(countryCode, continentId));
        Coordinates coordinates = getCoordinates(latitude, longitude);
        when(airport.getCoordinates()).thenReturn(coordinates);
        when(airport.getCity()).thenReturn(city);
        return airport;
    }

    private Country getCountry(String countryCode, int continentId) {
        Country country = getCountry(countryCode);
        Continent continent = getContinent(continentId);
        when(country.getContinent()).thenReturn(continent);
        return country;
    }

    private City getCity(Country country) {
        City city = mock(City.class);
        when(city.getCountry()).thenReturn(country);
        return city;
    }

    private Country getCountry(String countryCode) {
        Country country = mock(Country.class);
        when(country.getCountryCode()).thenReturn(countryCode);
        return country;
    }

    private Continent getContinent(int continentId) {
        Continent continent = mock(Continent.class);
        when(continent.getContinentId()).thenReturn(continentId);
        return continent;
    }

    private Coordinates getCoordinates(BigDecimal latitude, BigDecimal longitude) {
        Coordinates coordinates = mock(Coordinates.class);
        when(coordinates.getLatitude()).thenReturn(latitude);
        when(coordinates.getLongitude()).thenReturn(longitude);
        return coordinates;
    }
}
