package com.odigeo.itineraryratingfeatureaggregator.history;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.itineraryratingfeatureaggregator.bean.SearchCriteria;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.SearchCriteriaRequest;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Instant;
import java.time.ZonedDateTime;

import static com.odigeo.itineraryratingfeatureaggregator.ZoneDateTimeHelper.UTC;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class SearchCriteriaRequestFactoryTest {

    private static final Long SEARCH_ID = 1L;
    private static final Instant AN_INSTANT = Instant.ofEpochMilli(1583330525L);
    private static final Instant ANOTHER_INSTANT = Instant.ofEpochMilli(1583220025L);
    private static final Integer NUM_ADULTS = 3;
    private static final Integer NUM_CHILDREN = 2;

    private SearchCriteriaRequestFactory factory;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init();
        factory = ConfigurationEngine.getInstance(SearchCriteriaRequestFactory.class);
    }

    @Test
    public void testGetRequest() {
        SearchCriteria criteria = mock(SearchCriteria.class);
        when(criteria.getDepartureDate()).thenReturn(ANOTHER_INSTANT);
        when(criteria.getSearchDate()).thenReturn(AN_INSTANT);
        when(criteria.getNumAdults()).thenReturn(NUM_ADULTS);
        when(criteria.getNumChildren()).thenReturn(NUM_CHILDREN);

        SearchCriteriaRequest request = factory.getRequest(criteria, SEARCH_ID);

        assertEquals(request.getSearchId(), SEARCH_ID);
        assertEquals(request.getDepartureDateUTC(), ZonedDateTime.ofInstant(ANOTHER_INSTANT, UTC));
        assertEquals(request.getSearchDateUTC(), ZonedDateTime.ofInstant(AN_INSTANT, UTC));
        assertEquals(request.getNumberOfAdults(), NUM_ADULTS);
        assertEquals(request.getNumberOfChildren(), NUM_CHILDREN);
    }

}
