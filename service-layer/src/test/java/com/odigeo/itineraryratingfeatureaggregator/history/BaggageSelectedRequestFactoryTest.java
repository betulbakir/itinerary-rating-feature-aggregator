package com.odigeo.itineraryratingfeatureaggregator.history;

import com.odigeo.itineraryratingfeatureaggregator.bean.BaggageConditions;
import com.odigeo.itineraryratingfeatureaggregator.bean.BaggageDescriptor;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class BaggageSelectedRequestFactoryTest {

    private static final String CHECK_IN = "CHECK_IN";
    private static final String CABIN = "CABIN";

    @Test
    public void testWhenNoBaggage() {

        List<BaggageConditions> segmentConditions = new ArrayList<>();
        List<BaggageConditions> itineraryConditions = new ArrayList<>();

        BaggageConditions itineraryCondition = mock(BaggageConditions.class);
        itineraryConditions.add(itineraryCondition);

        BaggageConditions segmentCondition = mock(BaggageConditions.class);
        segmentConditions.add(segmentCondition);

        BaggageDescriptor itineraryDescriptor = getBaggageDescriptor(0, 0);
        when(itineraryCondition.getBaggageDescriptor()).thenReturn(itineraryDescriptor);

        BaggageDescriptor segmentDescriptor = getBaggageDescriptor(0, 0);
        when(segmentCondition.getBaggageDescriptor()).thenReturn(segmentDescriptor);

        BaggageSelectedRequestFactory factory = new BaggageSelectedRequestFactory();
        String baggageType = factory.getRequest(segmentConditions, itineraryConditions);

        assertEquals(baggageType, CABIN);
    }

    @Test
    public void testWhenItineraryConditionsWithKilos() {

        List<BaggageConditions> segmentConditions = new ArrayList<>();
        List<BaggageConditions> itineraryConditions = new ArrayList<>();

        BaggageConditions itineraryCondition = mock(BaggageConditions.class);
        itineraryConditions.add(itineraryCondition);

        BaggageConditions segmentCondition = mock(BaggageConditions.class);
        segmentConditions.add(segmentCondition);

        BaggageDescriptor itineraryDescriptor = getBaggageDescriptor(1, 0);
        when(itineraryCondition.getBaggageDescriptor()).thenReturn(itineraryDescriptor);

        BaggageDescriptor segmentDescriptor = getBaggageDescriptor(0, 0);
        when(segmentCondition.getBaggageDescriptor()).thenReturn(segmentDescriptor);

        BaggageSelectedRequestFactory factory = new BaggageSelectedRequestFactory();
        String baggageType = factory.getRequest(segmentConditions, itineraryConditions);

        assertEquals(baggageType, CHECK_IN);
    }

    @Test
    public void testWhenItineraryConditionsWithPieces() {

        List<BaggageConditions> segmentConditions = new ArrayList<>();
        List<BaggageConditions> itineraryConditions = new ArrayList<>();

        BaggageConditions itineraryCondition = mock(BaggageConditions.class);
        itineraryConditions.add(itineraryCondition);

        BaggageConditions segmentCondition = mock(BaggageConditions.class);
        segmentConditions.add(segmentCondition);

        BaggageDescriptor itineraryDescriptor = getBaggageDescriptor(0, 1);
        when(itineraryCondition.getBaggageDescriptor()).thenReturn(itineraryDescriptor);

        BaggageDescriptor segmentDescriptor = getBaggageDescriptor(0, 0);
        when(segmentCondition.getBaggageDescriptor()).thenReturn(segmentDescriptor);

        BaggageSelectedRequestFactory factory = new BaggageSelectedRequestFactory();
        String baggageType = factory.getRequest(segmentConditions, itineraryConditions);

        assertEquals(baggageType, CHECK_IN);
    }

    @Test
    public void testWhenSegmentConditionsWithKilos() {

        List<BaggageConditions> segmentConditions = new ArrayList<>();
        List<BaggageConditions> itineraryConditions = new ArrayList<>();

        BaggageConditions itineraryCondition = mock(BaggageConditions.class);
        itineraryConditions.add(itineraryCondition);

        BaggageConditions segmentCondition = mock(BaggageConditions.class);
        segmentConditions.add(segmentCondition);

        BaggageDescriptor itineraryDescriptor = getBaggageDescriptor(0, 0);
        when(itineraryCondition.getBaggageDescriptor()).thenReturn(itineraryDescriptor);

        BaggageDescriptor segmentDescriptor = getBaggageDescriptor(1, 0);
        when(segmentCondition.getBaggageDescriptor()).thenReturn(segmentDescriptor);

        BaggageSelectedRequestFactory factory = new BaggageSelectedRequestFactory();
        String baggageType = factory.getRequest(segmentConditions, itineraryConditions);

        assertEquals(baggageType, CHECK_IN);
    }

    @Test
    public void testWhenSegmentConditionsWithPieces() {

        List<BaggageConditions> segmentConditions = new ArrayList<>();
        List<BaggageConditions> itineraryConditions = new ArrayList<>();

        BaggageConditions itineraryCondition = mock(BaggageConditions.class);
        itineraryConditions.add(itineraryCondition);

        BaggageConditions segmentCondition = mock(BaggageConditions.class);
        segmentConditions.add(segmentCondition);

        BaggageDescriptor itineraryDescriptor = getBaggageDescriptor(0, 0);
        when(itineraryCondition.getBaggageDescriptor()).thenReturn(itineraryDescriptor);

        BaggageDescriptor segmentDescriptor = getBaggageDescriptor(0, 1);
        when(segmentCondition.getBaggageDescriptor()).thenReturn(segmentDescriptor);

        BaggageSelectedRequestFactory factory = new BaggageSelectedRequestFactory();
        String baggageType = factory.getRequest(segmentConditions, itineraryConditions);

        assertEquals(baggageType, CHECK_IN);
    }

    private BaggageDescriptor getBaggageDescriptor(int numKilos, int numPieces) {
        BaggageDescriptor itineraryDescriptor = mock(BaggageDescriptor.class);
        when(itineraryDescriptor.getNumKilos()).thenReturn(numKilos);
        when(itineraryDescriptor.getNumPieces()).thenReturn(numPieces);
        return itineraryDescriptor;
    }
}
