package com.odigeo.itineraryratingfeatureaggregator.history;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryratingfeatureaggregator.bean.ItineraryShoppingItem;
import com.odigeo.itineraryratingfeatureaggregator.bean.MessageAggregatedData;
import com.odigeo.itineraryratingfeatureaggregator.bean.ModelEvaluationRequested;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.ItinerarySelectedRequest;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.SearchCriteriaRequest;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.UserSelectionRequest;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.Map;

import static com.odigeo.itineraryratingfeatureaggregator.ZoneDateTimeHelper.UTC;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class UserSelectionRequestFactoryTest {

    private static final String USER_COOKIE = "COOKIE";
    private static final Instant AN_INSTANT = Instant.ofEpochMilli(1583330525L);

    @Mock
    private SearchCriteriaRequestFactory searchCriteriaRequestFactory;
    @Mock
    private ItinerarySelectedRequestFactory itinerarySelectedRequestFactory;
    @Mock
    private ModelStatisticsRequestFactory modelStatisticsRequestFactory;
    @Mock
    private SearchCriteriaRequest searchCriteriaRequest;
    @Mock
    private ItinerarySelectedRequest itinerarySelectedRequest;
    @Mock
    private Map<String, Number> modelStatistics;

    private UserSelectionRequestFactory factory;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        searchCriteriaRequest = mock(SearchCriteriaRequest.class, Mockito.withSettings().withoutAnnotations());
        searchCriteriaRequestFactory = mock(SearchCriteriaRequestFactory.class, Mockito.withSettings().withoutAnnotations());
        itinerarySelectedRequestFactory = mock(ItinerarySelectedRequestFactory.class, Mockito.withSettings().withoutAnnotations());
        modelStatisticsRequestFactory = mock(ModelStatisticsRequestFactory.class, Mockito.withSettings().withoutAnnotations());

        ConfigurationEngine.init(new AbstractModule() {
            public void configure() {
                bind(SearchCriteriaRequestFactory.class).toInstance(searchCriteriaRequestFactory);
                bind(ItinerarySelectedRequestFactory.class).toInstance(itinerarySelectedRequestFactory);
                bind(ModelStatisticsRequestFactory.class).toInstance(modelStatisticsRequestFactory);
            }
        });
        factory = ConfigurationEngine.getInstance(UserSelectionRequestFactory.class);
        when(searchCriteriaRequestFactory.getRequest(any(), any())).thenReturn(searchCriteriaRequest);
        when(itinerarySelectedRequestFactory.getRequest(any(ItineraryShoppingItem.class))).thenReturn(itinerarySelectedRequest);
        when(modelStatisticsRequestFactory.getRequest(any(ModelEvaluationRequested.class))).thenReturn(modelStatistics);
    }

    @Test
    public void testGetRequest() {

        MessageAggregatedData data = mock(MessageAggregatedData.class);
        ModelEvaluationRequested modelEvaluationRequested = mock(ModelEvaluationRequested.class);
        ItineraryShoppingItem itemAdded = mock(ItineraryShoppingItem.class);
        when(data.getUniqueUserCookieId()).thenReturn(USER_COOKIE);
        when(data.getItemAddedOccurrence()).thenReturn(AN_INSTANT);
        when(data.getModelEvaluation()).thenReturn(modelEvaluationRequested);
        when(data.getItineraryItemAdded()).thenReturn(itemAdded);

        UserSelectionRequest request = factory.getRequest(data);

        assertEquals(request.getSelectionDateUTC(), AN_INSTANT.atZone(UTC));
        assertEquals(request.getUserDevice(), USER_COOKIE);
        assertEquals(request.getItinerarySelectedRequest(), itinerarySelectedRequest);
        assertEquals(request.getModelStatistics(), modelStatistics);
        assertEquals(request.getSearchCriteriaRequest(), searchCriteriaRequest);
    }
}
