package com.odigeo.itineraryratingfeatureaggregator.history;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryratingfeatureaggregator.bean.Itinerary;
import com.odigeo.itineraryratingfeatureaggregator.bean.ItineraryShoppingItem;
import com.odigeo.itineraryratingfeatureaggregator.bean.Segment;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.ItinerarySelectedRequest;
import com.odigeo.itineraryratingfeaturehistory.api.v1.contract.requests.SegmentRequest;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class ItinerarySelectedRequestFactoryTest {

    private static final BigDecimal ONE = BigDecimal.ONE;

    @Mock
    private SegmentSelectedRequestFactory segmentRequestFactory;
    @Mock
    private SegmentRequest segmentRequest;

    private ItinerarySelectedRequestFactory factory;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            public void configure() {
                bind(SegmentSelectedRequestFactory.class).toInstance(segmentRequestFactory);
            }
        });
        factory = ConfigurationEngine.getInstance(ItinerarySelectedRequestFactory.class);
        when(segmentRequestFactory.getRequest(any(), any())).thenReturn(segmentRequest);
    }

    @Test
    public void testGetRequest() {

        ItineraryShoppingItem itemAdded = mock(ItineraryShoppingItem.class);
        Itinerary itinerary = mock(Itinerary.class);
        when(itemAdded.getItinerary()).thenReturn(itinerary);
        when(itinerary.getApparentPriceEur()).thenReturn(ONE);
        when(itinerary.isHasFreeCancellation()).thenReturn(true);

        Segment segment = mock(Segment.class);
        List<Segment> value = new ArrayList<>();
        value.add(segment);
        value.add(segment);
        when(itinerary.getSegments()).thenReturn(value);

        ItinerarySelectedRequest request = factory.getRequest(itemAdded);

        assertEquals(request.getApparentPriceInEur(), ONE);
        assertEquals(request.getFreeCancellation(), Boolean.TRUE);
        assertEquals(request.getSegmentsRequest().size(), 2);
        assertEquals(request.getSegmentsRequest().get(0), segmentRequest);
        assertEquals(request.getSegmentsRequest().get(1), segmentRequest);
    }
}
