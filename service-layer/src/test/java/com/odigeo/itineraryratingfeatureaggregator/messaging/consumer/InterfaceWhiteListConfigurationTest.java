package com.odigeo.itineraryratingfeatureaggregator.messaging.consumer;

import com.odigeo.itineraryratingfeatureaggregator.Interface;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.testng.Assert.assertEquals;

public class InterfaceWhiteListConfigurationTest {

    private InterfaceWhiteListConfiguration configuration;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        configuration = new InterfaceWhiteListConfiguration();
    }

    @Test
    public void testSetWhiteListedInterfaceName() {
        Interface anInterface = Interface.ONE_FRONT_DESKTOP;
        Interface anotherInterface = Interface.NATIVE_ANDROID_EDREAMS;
        Integer[] interfaceNames = {anInterface.getId(), anotherInterface.getId() };

        configuration.setWhiteListedInterfaceIds(interfaceNames);
        Set<Integer> actualInterfaceIds =  configuration.getWhiteList();

        Set<Integer> expectedInterfaceIds = new HashSet<>(Arrays.asList(anInterface.getId(), anotherInterface.getId()));

        assertEquals(actualInterfaceIds, expectedInterfaceIds);
    }

}
