package com.odigeo.itineraryratingfeatureaggregator.messaging.consumer;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.commons.messaging.domain.events.DomainEvent;
import com.odigeo.itineraryratingfeatureaggregator.cache.CacheKey;
import com.odigeo.itineraryratingfeatureaggregator.cache.ModelEvaluationCache;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsConstants;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsHandler;
import com.odigeo.retail.meitineraryrating.request.domainevent.ModelEvaluationRequested;
import org.apache.avro.specific.SpecificRecord;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ModelEvaluationRequestedProcessorTest {

    @Mock
    private ModelEvaluationCache cache;
    @Mock
    private MetricsHandler metricsHandler;

    private ModelEvaluationRequestedProcessor processor;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        cache = mock(ModelEvaluationCache.class, Mockito.withSettings().withoutAnnotations());
        metricsHandler = mock(MetricsHandler.class, Mockito.withSettings().withoutAnnotations());
        ConfigurationEngine.init(new AbstractModule() {
            public void configure() {
                bind(ModelEvaluationCache.class).toInstance(cache);
                bind(MetricsHandler.class).toInstance(metricsHandler);
            }
        });
        processor = ConfigurationEngine.getInstance(ModelEvaluationRequestedProcessor.class);
    }

    @Test
    public void testOnMessage() {
        DomainEvent<SpecificRecord> domainEvent = mock(DomainEvent.class);
        ModelEvaluationRequested evaluationRequested = mock(ModelEvaluationRequested.class);
        when(domainEvent.getPayload()).thenReturn(evaluationRequested);
        processor.onMessage(domainEvent);
        verify(cache).addEntry(any(CacheKey.class), eq(evaluationRequested));
        verify(metricsHandler).counterMetric(MetricsConstants.MESSAGE_PROCESSED_ME_ITINERARY_RATING.getName());
    }
}
