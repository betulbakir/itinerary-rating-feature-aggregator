package com.odigeo.itineraryratingfeatureaggregator.messaging.consumer;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.commons.messaging.domain.events.DomainEvent;
import com.odigeo.commons.messaging.domain.events.Metadata;
import com.odigeo.itineraryratingfeatureaggregator.bean.MessageAggregatedData;
import com.odigeo.itineraryratingfeatureaggregator.cache.CacheKey;
import com.odigeo.itineraryratingfeatureaggregator.cache.CacheKeyType;
import com.odigeo.itineraryratingfeatureaggregator.cache.ModelEvaluationCache;
import com.odigeo.itineraryratingfeatureaggregator.cache.VisitCache;
import com.odigeo.itineraryratingfeatureaggregator.history.ItineraryRatingFeatureHistoryHandler;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsConstants;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsHandler;
import com.odigeo.retail.edreamssite.shoppingcart.domainevents.ItineraryShoppingItem;
import com.odigeo.retail.edreamssite.shoppingcart.domainevents.ShoppingItemAdded;
import com.odigeo.retail.meitineraryrating.request.domainevent.ModelEvaluationRequested;
import ma.glasnost.orika.MapperFacade;
import org.apache.avro.specific.SpecificRecord;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ShoppingItemAddedProcessorTest {

    private static final long VISIT_ID = 1L;
    private static final long SEARCH_ID = 2L;
    private static final String USER_COOKIE = "USER_COOKIE";

    @Mock
    private VisitCache visitCache;
    @Mock
    private ModelEvaluationCache modelEvaluationCache;
    @Mock
    private MetricsHandler metricsHandler;
    @Mock
    private ItineraryRatingFeatureHistoryHandler handler;
    @Mock
    private MapperFacade mapperFacade;

    private ShoppingItemAddedProcessor processor;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        visitCache = mock(VisitCache.class, Mockito.withSettings().withoutAnnotations());
        modelEvaluationCache = mock(ModelEvaluationCache.class, Mockito.withSettings().withoutAnnotations());
        metricsHandler = mock(MetricsHandler.class, Mockito.withSettings().withoutAnnotations());
        handler = mock(ItineraryRatingFeatureHistoryHandler.class, Mockito.withSettings().withoutAnnotations());
        mapperFacade = mock(MapperFacade.class, Mockito.withSettings().withoutAnnotations());
        ConfigurationEngine.init(new AbstractModule() {
            public void configure() {
                bind(VisitCache.class).toInstance(visitCache);
                bind(ModelEvaluationCache.class).toInstance(modelEvaluationCache);
                bind(MetricsHandler.class).toInstance(metricsHandler);
                bind(ItineraryRatingFeatureHistoryHandler.class).toInstance(handler);
                bind(MapperFacade.class).toInstance(mapperFacade);
            }
        });
        processor = ConfigurationEngine.getInstance(ShoppingItemAddedProcessor.class);
    }

    @Test
    public void testOnMessage() {

        DomainEvent<SpecificRecord> domainEvent = getDomainEvent();

        CacheKey visitKey = new CacheKey(CacheKeyType.VISIT_ID, VISIT_ID);
        when(visitCache.fetchValue(visitKey)).thenReturn(USER_COOKIE);

        ModelEvaluationRequested modelEvaluation = mock(ModelEvaluationRequested.class);
        CacheKey searchKey = new CacheKey(CacheKeyType.SEARCH_ID, SEARCH_ID);
        when(modelEvaluationCache.fetchValue(searchKey)).thenReturn(modelEvaluation);

        processor.onMessage(domainEvent);

        verify(metricsHandler).sendCounterMetric(MetricsConstants.MESSAGE_CONSUMED_ITEM_ADDED);
        verify(metricsHandler).sendCounterMetric(MetricsConstants.MESSAGE_PROCESSED_ITEM_ADDED);
        verify(handler).sendUserData(any(MessageAggregatedData.class));
        verify(metricsHandler, never()).sendCounterMetric(MetricsConstants.MODEL_EVALUATION_NOT_FOUND);
        verify(metricsHandler, never()).sendCounterMetric(MetricsConstants.USER_COOKIE_NOT_FOUND);
    }


    @Test
    public void testOnMessageNoUserCookieCached() {

        DomainEvent<SpecificRecord> domainEvent = getDomainEvent();

        String notCachedUserCookie = null;
        CacheKey visitKey = new CacheKey(CacheKeyType.VISIT_ID, VISIT_ID);
        when(visitCache.fetchValue(visitKey)).thenReturn(notCachedUserCookie);

        ModelEvaluationRequested modelEvaluation = mock(ModelEvaluationRequested.class);
        CacheKey searchKey = new CacheKey(CacheKeyType.SEARCH_ID, SEARCH_ID);
        when(modelEvaluationCache.fetchValue(searchKey)).thenReturn(modelEvaluation);

        processor.onMessage(domainEvent);

        verify(metricsHandler).sendCounterMetric(MetricsConstants.MESSAGE_CONSUMED_ITEM_ADDED);
        verify(metricsHandler).sendCounterMetric(MetricsConstants.USER_COOKIE_NOT_FOUND);
        verify(metricsHandler, never()).sendCounterMetric(MetricsConstants.MESSAGE_PROCESSED_ITEM_ADDED);
        verify(metricsHandler, never()).sendCounterMetric(MetricsConstants.MODEL_EVALUATION_NOT_FOUND);
    }

    @Test
    public void testOnMessageNoModelEvaluationCached() {

        DomainEvent<SpecificRecord> domainEvent = getDomainEvent();

        CacheKey visitKey = new CacheKey(CacheKeyType.VISIT_ID, VISIT_ID);
        when(visitCache.fetchValue(visitKey)).thenReturn(USER_COOKIE);

        ModelEvaluationRequested notCachedModelEvaluation = null;
        CacheKey searchKey = new CacheKey(CacheKeyType.SEARCH_ID, SEARCH_ID);
        when(modelEvaluationCache.fetchValue(searchKey)).thenReturn(notCachedModelEvaluation);

        processor.onMessage(domainEvent);

        verify(metricsHandler).sendCounterMetric(MetricsConstants.MESSAGE_CONSUMED_ITEM_ADDED);
        verify(metricsHandler).sendCounterMetric(MetricsConstants.MODEL_EVALUATION_NOT_FOUND);
        verify(metricsHandler, never()).sendCounterMetric(MetricsConstants.USER_COOKIE_NOT_FOUND);
        verify(metricsHandler, never()).sendCounterMetric(MetricsConstants.MESSAGE_PROCESSED_ITEM_ADDED);
    }

    private DomainEvent<SpecificRecord> getDomainEvent() {
        DomainEvent<SpecificRecord> domainEvent = mock(DomainEvent.class);
        ShoppingItemAdded itemAdded = mock(ShoppingItemAdded.class);
        Metadata metadata = mock(Metadata.class);
        ItineraryShoppingItem item = mock(ItineraryShoppingItem.class);
        when(domainEvent.getPayload()).thenReturn(itemAdded);
        when(domainEvent.getMetadata()).thenReturn(metadata);
        when(itemAdded.getItineraryShoppingItem()).thenReturn(item);
        when(item.getSearchId()).thenReturn(SEARCH_ID);
        when(item.getVisitId()).thenReturn(VISIT_ID);
        return domainEvent;
    }

}
