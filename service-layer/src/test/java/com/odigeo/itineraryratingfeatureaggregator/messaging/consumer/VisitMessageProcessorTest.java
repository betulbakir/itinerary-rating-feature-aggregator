package com.odigeo.itineraryratingfeatureaggregator.messaging.consumer;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryratingfeatureaggregator.cache.CacheKey;
import com.odigeo.itineraryratingfeatureaggregator.cache.VisitCache;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsConstants;
import com.odigeo.itineraryratingfeatureaggregator.metrics.MetricsHandler;
import com.odigeo.visit.message.v1.VisitMessage;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class VisitMessageProcessorTest {

    private static final Integer ALLOWED_INTERFACE_ID = 16;
    private static final Integer NOT_ALLOWED_INTERFACE_ID = -1;
    private static final String VALID_USER_COOKIE = "dbf2edfd-eb48-4580-b8ef-48c99eb01b42";
    private static final String INVALID_USER_COOKIE = "invalidUserCookie";

    @Mock
    private VisitCache cache;
    @Mock
    private InterfaceWhiteListConfiguration configuration;
    @Mock
    private UserCookieValidator validator;
    @Mock
    private MetricsHandler metricsHandler;

    private VisitMessageProcessor processor;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        cache = mock(VisitCache.class, Mockito.withSettings().withoutAnnotations());
        configuration = mock(InterfaceWhiteListConfiguration.class, Mockito.withSettings().withoutAnnotations());
        metricsHandler = mock(MetricsHandler.class, Mockito.withSettings().withoutAnnotations());
        validator = mock(UserCookieValidator.class, Mockito.withSettings().withoutAnnotations());
        ConfigurationEngine.init(new AbstractModule() {
            public void configure() {
                bind(VisitCache.class).toInstance(cache);
                bind(InterfaceWhiteListConfiguration.class).toInstance(configuration);
                bind(MetricsHandler.class).toInstance(metricsHandler);
                bind(UserCookieValidator.class).toInstance(validator);
            }
        });
        Set<Integer> interfaceIds = new HashSet<>(Arrays.asList(ALLOWED_INTERFACE_ID));
        when(configuration.getWhiteList()).thenReturn(interfaceIds);
        processor = ConfigurationEngine.getInstance(VisitMessageProcessor.class);
    }

    @Test
    public void testOnMessageWhenAllowedInterfaceAndValidUserCookie() {
        VisitMessage visitMessage = getVisitMessageWith(VALID_USER_COOKIE, ALLOWED_INTERFACE_ID);

        processor.onMessage(visitMessage);

        assertMessageProcessed();
    }

    @Test
    public void testOnMessageWhenAllowedInterfaceAndInvalidUserCookie() {
        VisitMessage visitMessage = getVisitMessageWith(INVALID_USER_COOKIE, ALLOWED_INTERFACE_ID);

        processor.onMessage(visitMessage);

        assertMessageNotProcessed(INVALID_USER_COOKIE);
    }

    @Test
    public void testOnMessageWhenNotAllowedInterfaceAndValidUserCookieId() {
        VisitMessage visitMessage = getVisitMessageWith(VALID_USER_COOKIE, NOT_ALLOWED_INTERFACE_ID);

        processor.onMessage(visitMessage);

        assertMessageNotProcessed(VALID_USER_COOKIE);
    }

    @Test
    public void testOnMessageWhenNotAllowedInterfaceAndInvalidUserCookieId() {
        VisitMessage visitMessage = getVisitMessageWith(INVALID_USER_COOKIE, NOT_ALLOWED_INTERFACE_ID);

        processor.onMessage(visitMessage);

        assertMessageNotProcessed(INVALID_USER_COOKIE);
    }

    private VisitMessage getVisitMessageWith(String cookieId, Integer interfaceId) {
        VisitMessage visitMessage = mock(VisitMessage.class);
        when(visitMessage.getUniqueUserCookieId()).thenReturn(cookieId);
        when(visitMessage.getInterfaceId()).thenReturn(interfaceId);
        when(validator.isValidUserCookieId(cookieId)).thenReturn(VALID_USER_COOKIE.equals(cookieId));
        return visitMessage;
    }

    private void assertMessageProcessed() {
        verify(cache).addEntry(any(CacheKey.class), eq(VALID_USER_COOKIE));
        verify(metricsHandler).counterMetric(MetricsConstants.MESSAGE_CONSUMED_VISIT.getName());
        verify(metricsHandler).counterMetric(MetricsConstants.MESSAGE_PROCESSED_VISIT.getName());
    }

    private void assertMessageNotProcessed(String invalidUserCookie) {
        verify(metricsHandler).counterMetric(MetricsConstants.MESSAGE_CONSUMED_VISIT.getName());
        verify(cache, never()).addEntry(any(CacheKey.class), eq(invalidUserCookie));
        verify(metricsHandler, never()).counterMetric(MetricsConstants.MESSAGE_PROCESSED_VISIT.getName());
    }
}
