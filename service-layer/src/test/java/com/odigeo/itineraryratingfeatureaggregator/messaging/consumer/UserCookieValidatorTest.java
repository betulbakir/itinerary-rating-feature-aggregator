package com.odigeo.itineraryratingfeatureaggregator.messaging.consumer;

import com.edreams.configuration.ConfigurationEngine;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class UserCookieValidatorTest {

    private UserCookieValidator validator;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init();
        validator = ConfigurationEngine.getInstance(UserCookieValidator.class);
    }

    @Test
    public void testIsValidUserCookieId() {
        assertTrue(validator.isValidUserCookieId("0d4593ec-d084-42f0-b444-efce857e8402"));
    }

    @Test
    public void testInvalidUserCookieId() {
        assertFalse(validator.isValidUserCookieId("no-valid-id"));
    }

    @Test
    public void testNullUserCookieId() {
        assertFalse(validator.isValidUserCookieId(null));
    }

}

