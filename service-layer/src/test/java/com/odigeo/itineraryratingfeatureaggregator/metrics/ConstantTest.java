package com.odigeo.itineraryratingfeatureaggregator.metrics;

import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;

public class ConstantTest {

    private static final String DEVICE_TAG = "<DEVICE>";
    private static final String DEVICE = "DESKTOP";

    Map<String, String> tagValues;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        tagValues = new HashMap<>();
        tagValues.put(DEVICE_TAG, DEVICE);
    }

    @Test
    public void testGetMetricName() {
        assertEquals(MetricsConstants.getMetricName(DEVICE_TAG, tagValues), DEVICE);
    }

}
