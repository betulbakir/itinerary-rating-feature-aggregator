package com.odigeo.itineraryratingfeatureaggregator.metrics;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static org.mockito.Mockito.spy;

public class MetricsHandlerTest {

    private static final String METRIC_KEY = "request";
    private static final String FAKE_METRIC_KEY = "request_fake";

    @Mock
    Map<String, String> tagValues;

    private MetricsHandler metricsHandler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        metricsHandler = new MetricsHandler();
    }

    @Test
    public void testCounterMetric() {
        MetricsHandler metricsHandlerSpyed = spy(metricsHandler);
        metricsHandlerSpyed.counterMetric(METRIC_KEY, tagValues);
        //The test is to see that there is no exception, so no Assert is needed
    }

    @Test
    public void testCounterMetricFakeConstant() {
        MetricsHandler metricsHandlerSpyed = spy(metricsHandler);
        metricsHandlerSpyed.counterMetric(FAKE_METRIC_KEY, tagValues);
        //The test is to see that there is no exception, so no Assert is needed
    }

    @Test
    public void testCounterMetricNoTags() {
        MetricsHandler metricsHandlerSpyed = spy(metricsHandler);
        metricsHandlerSpyed.counterMetric(METRIC_KEY);
        //The test is to see that there is no exception, so no Assert is needed
    }

    @Test
    public void testStartTimerMetricFakeConstant() {
        MetricsHandler metricsHandlerSpyed = spy(metricsHandler);
        metricsHandlerSpyed.startTimer(FAKE_METRIC_KEY, tagValues);
        //The test is to see that there is no exception, so no Assert is needed
    }

    @Test
    public void testStartTimerNoTags() {
        MetricsHandler metricsHandlerSpyed = spy(metricsHandler);
        metricsHandlerSpyed.startTimer(METRIC_KEY);
        //The test is to see that there is no exception, so no Assert is needed
    }

    @Test
    public void testStopTimerMetricFakeConstant() {
        MetricsHandler metricsHandlerSpyed = spy(metricsHandler);
        metricsHandlerSpyed.stopTimer(FAKE_METRIC_KEY, tagValues);
        //The test is to see that there is no exception, so no Assert is needed
    }

    @Test
    public void testStopTimerNoTags() {
        MetricsHandler metricsHandlerSpyed = spy(metricsHandler);
        metricsHandlerSpyed.stopTimer(METRIC_KEY);
        //The test is to see that there is no exception, so no Assert is needed
    }
}
