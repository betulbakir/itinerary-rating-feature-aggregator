package com.odigeo.itineraryratingfeatureaggregator.metrics;

import com.odigeo.commons.monitoring.metrics.Metric;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class MetricsConstantsTest {

    private static final String TAG_KEY = "key";
    private static final String TAG_VALUE = "value";
    private static final String TAG_WEBSITE_KEY = "website";
    private static final String TAG_WEBSITE_VALUE = "<WEBSITE>";
    private static final String WEBSITE = "ES";
    private static final String METRIC_PRECISION = "pMINUTELY" + Metric.SEPARATOR + "rWEEK";
    private static final String METRIC_NAME = "name";
    private static final String METRIC_WEBSITE = TAG_WEBSITE_KEY + Metric.SEPARATOR + WEBSITE;

    @Test
    public void testConstantBasicBuilder() {
        Constant constant = new Constant.Builder(METRIC_NAME).build();

        assertEquals(constant.getName(), METRIC_NAME);
    }

    @Test
    public void testGetMetric() {
        Constant constant = new Constant.Builder(METRIC_NAME).tag(TAG_WEBSITE_KEY, TAG_WEBSITE_VALUE).build();

        Map<String, String> tagValues = new HashMap<>();
        tagValues.put(TAG_WEBSITE_VALUE, WEBSITE);

        Metric metric = constant.getMetric(tagValues);

        assertEquals(metric.getId(), METRIC_WEBSITE + Metric.SEPARATOR + METRIC_NAME + Metric.SEPARATOR + METRIC_PRECISION);
    }

    @Test
    public void testGetMetricWithEmptyTags() {
        Constant constant = new Constant.Builder(METRIC_NAME).build();
        Metric metric = constant.getMetric(null);

        assertEquals(metric.getId(), METRIC_NAME + Metric.SEPARATOR + METRIC_PRECISION);
        assertNull(metric.getBuckets());
    }

}
