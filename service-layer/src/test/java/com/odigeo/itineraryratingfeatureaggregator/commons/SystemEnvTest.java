package com.odigeo.itineraryratingfeatureaggregator.commons;

import org.testng.annotations.Test;

import java.util.Optional;

import static org.testng.Assert.assertFalse;

public class SystemEnvTest {

    @Test
    public void testGet() {
        SystemEnv systemEnv = new SystemEnv();
        String variable = "envVariableNotPresent";
        Optional<String> actualOpt =  systemEnv.get(variable);
        assertFalse(actualOpt.isPresent());
    }
}
