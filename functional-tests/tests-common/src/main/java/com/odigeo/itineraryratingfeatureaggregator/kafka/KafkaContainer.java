package com.odigeo.itineraryratingfeatureaggregator.kafka;

import com.google.inject.Singleton;
import com.odigeo.technology.docker.ContainerException;
import com.odigeo.technology.docker.ContainerInfo;
import com.odigeo.technology.docker.ContainerInfoBuilder;
import com.odigeo.technology.docker.SocketPingChecker;

@Singleton
public class KafkaContainer {

    public ContainerInfo createContainer() throws ContainerException {
        ContainerInfo zookeeperContainerInfo = new ContainerInfoBuilder("confluentinc/cp-zookeeper:3.3.1")
                .setName("zookeeper")
                .addPortMapping(2181, 2181)
                .addEvironmentVariable("ZOOKEEPER_CLIENT_PORT", "2181")
                .setPingChecker(new SocketPingChecker("localhost", 2181))
                .build();

        return new ContainerInfoBuilder("confluentinc/cp-kafka:5.5.1")
                .setName("kafka")
                .addDependency(zookeeperContainerInfo)
                .addPortMapping(9092, 9092)
                .addEvironmentVariable("KAFKA_ZOOKEEPER_CONNECT", "zookeeper:2181")
                .addEvironmentVariable("KAFKA_BROKER_ID", "1")
                .addEvironmentVariable("KAFKA_LISTENERS", "DOCKER://0.0.0.0:29092,LOCALHOST://0.0.0.0:9092")
                .addEvironmentVariable("KAFKA_ADVERTISED_LISTENERS", "DOCKER://kafka:29092,LOCALHOST://localhost:9092")
                .addEvironmentVariable("KAFKA_LISTENER_SECURITY_PROTOCOL_MAP", "DOCKER:PLAINTEXT,LOCALHOST:PLAINTEXT")
                .addEvironmentVariable("KAFKA_INTER_BROKER_LISTENER_NAME", "DOCKER")
                .addEvironmentVariable("KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR", "1")
                .addEvironmentVariable("KAFKA_AUTO_CREATE_TOPICS_ENABLE", "true")
                .setPingChecker(new SocketPingChecker("localhost", 9092))
                .build();
    }

}
