package com.odigeo.itineraryratingfeatureaggregator;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

@Singleton
@ConfiguredInPropertiesFile
public class FunctionalTestConfiguration {

    private String localIp;

    public String getLocalIp() {
        return localIp;
    }

    public void setLocalIp(String localIp) {
        this.localIp = localIp;
    }

}
