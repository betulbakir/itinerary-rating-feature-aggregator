package com.odigeo.itineraryratingfeatureaggregator.functionals;

public class FunctionalException extends RuntimeException {
    public FunctionalException(Throwable cause) {
        super(cause);
    }
}
