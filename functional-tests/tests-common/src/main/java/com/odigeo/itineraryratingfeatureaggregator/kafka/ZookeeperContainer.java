package com.odigeo.itineraryratingfeatureaggregator.kafka;

import com.google.inject.Singleton;
import com.odigeo.technology.docker.ContainerException;
import com.odigeo.technology.docker.ContainerInfo;
import com.odigeo.technology.docker.ContainerInfoBuilder;
import com.odigeo.technology.docker.SocketPingChecker;

@Singleton
public class ZookeeperContainer {

    static final int PORT = 2181;
    private static final String IMAGE = "wurstmeister/zookeeper:3.4.6";
    private static final String HOST = "localhost";
    private static final String NAME = "zookeeper-functional-test";

    public ContainerInfo createContainer() throws ContainerException {
        return new ContainerInfoBuilder(IMAGE)
                .setName(NAME)
                .addPortMapping(PORT, PORT)
                .setPingChecker(new SocketPingChecker(HOST, PORT))
                .build();
    }

}

